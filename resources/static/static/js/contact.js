var Skngp = Skngp || {};

Skngp.Contact = (function (ns, $) {
  var $body = null;

  function bindReady() {
    var def = jQuery.Deferred();
    $body = $('body');
    $body.addClass('jsContactModule');
    
    $(document).ready(function () {
      ns.loadMap();
      def.resolve('true');
    });

    return def.promise();
  }

  ns.process = function (callback) {
    console.log('go!');
    $.when(bindReady()).then(function () {
      if (typeof callback === 'function') {
        callback();
      }
    });
  };

  ns.loadMap = function () {
    var map;
    console.log('load map');
    if ($('#map').length > 0) {
      require(['esri/map', 'dojo/domReady!'], function() {
        map = new Map('map', {
          center: [21.046297203505304, 52.162012565152779],
          zoom: 16,
          basemap: 'topo'
        });
        console.log('required!');
      });
    }
  };

  ns.test = function () {
    return $body.hasClass('jsContactModule');
  };

  return ns;
}(Skngp.Contact || {}, jQuery.noConflict()));