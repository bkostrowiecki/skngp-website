var Skngp = Skngp || {};
Skngp.Library = {};

Skngp.Library = (function (ns, $) {
  var $body = null;

  function clearOtherColumns($obj) {
    $('.jsLibraryTable th').each(function() {
      var $this = $(this);
      var $icon = $this.find('.jsLibraryTableSortIcon');
      if ($obj.data('column') !== $this.data('column')) {
        $icon.hide();
      } else {
        $icon.show();
      }
    });
  }

  function processIcon($obj) {
    var $icon = null;
    if (!$obj.hasClass('jsLibraryTableSort')) {
      $obj.addClass('jsLibraryTableSort');
      $icon = $('<i class="glyphicon glyphicon-triangle-top jsLibraryTableSortIcon" />');
      $obj.append($icon);
      $obj[0].click();
    } else {
      $icon = $obj.find('.jsLibraryTableSortIcon');
      $icon.toggleClass('glyphicon-triangle-top');
      $icon.toggleClass('glyphicon-triangle-bottom');
      clearOtherColumns($obj);
    }
  }

  function bindEvents() {
    $('.jsLibraryTable').tablesorter();

    $('.jsLibraryTable th').each(function() {
      $(this).on('click', function() {
        processIcon($(this));
      });
      $(this).addClass('cursor-pointer');
    });
  }

  function bindReady() {
    var def = jQuery.Deferred();

    $(document).ready(function () {
      $body = $('body');
      $body.addClass('jsLibraryModule');
      bindEvents();

      def.resolve('true');
    });

    return def.promise();
  }

  ns.process = function (callback) {
    $.when(bindReady()).then(function () {

      if (typeof callback === 'function') {
        callback();
      }
    });
  };

  ns.test = function () {
    return $body.hasClass('jsLibraryModule');
  };

  return ns;
}(Skngp.Library || {}, jQuery.noConflict()));