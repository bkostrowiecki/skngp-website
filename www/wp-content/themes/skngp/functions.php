<?php

require_once( 'inc/helpers.php' );
require_once( 'inc/members.php' );

add_action('after_setup_theme', 'skngp_setup_language');
function skngp_setup_language(){
  if (! load_theme_textdomain( 'SKNGP SGGW', get_template_directory() . '/languages' ) ) {
    echo 'CRAP';
  }
}

/**
 * Fires top menu rendering. Use it like wp_nav
 * 
 * @since Skngp 1.0.0
 * 
 * @param  string $menu_name Name of menu
 * @return void
 */
function skngp_top_menu( $menu_name = null ) {
  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items( $menu->term_id );
    $categoryUrlMatch = get_option( 'category_url_match' );
    
    ?>
      <ul id="menu-<?php echo $menu_name; ?>" class="list list--main-nav nav navbar-nav">
        <?php 
        foreach ( (array) $menu_items as $key => $menu_item ) : 
          if ( "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] === $menu_item->url ) {
            $modifier = 'list--main-nav__item--active';
          } else if ( $categoryUrlMatch !== false && preg_match( $categoryUrlMatch, $menu_item->url ) === 1 && preg_match( $categoryUrlMatch, "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] ) === 1 ) {
            $modifier = 'list--main-nav__item--active';
          } else {
            $modifier = '';
          }
          ?>
        <li class="list--main-nav__item <?php echo $modifier; ?>">
          <a class="link link--main-nav" href="<?php echo $menu_item->url; ?>" title="<?php echo $menu_item->attr_title; ?>">
            <?php echo $menu_item->title; ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
    <?php
  }
}

function skngp_footer_menu( $menu_name = null ) {
  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items( $menu->term_id );
    if ( $menu->name == '' ) {
      return;
    }
    ?>
    <h4 class="heading heading--sitemap"><?php echo $menu->name; ?></h4>
    <ul id="menu-<?php echo $menu_name; ?>" class="list list--unstyled list--sitemap">
    <?php
      foreach ( (array) $menu_items as $key => $menu_item ) : ?>
      <li class="list--sitemap__item">
        <a href="<?php echo $menu_item->url; ?>" class="link link--footer" title="<?php echo $menu_item->attr_title; ?>">
          <?php echo $menu_item->title; ?>
        </a>
      </li>
      <?php endforeach; ?>
    </ul>
    <?php 
  }
}

function skngp_setup_customization( $wp_customize ) {
  /*
   * Subtitle
   */
  $wp_customize->add_setting( 'subtitle', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'subtitle_control', array(
    'label' => __( 'Subtitle', 'SKNGP SGGW' ),
    'section' => 'title_tagline',
    'settings' => 'subtitle',
    'type' => 'options'
  ) );

  /*
   * Address section
   */
  $wp_customize->add_section( 'address_section', array(
    'title' => __('Address informations', 'SKNGP SGGW' ),
    'priority' => 30
  ) );

  $wp_customize->add_setting( 'postal_code', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'locality', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'address', array(
    'default' => '02-787 Warszawa<br>ul. Nowoursynowska 159',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'additional_info', array(
    'default' => 'budynek nr 34 p. 0/55',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'postal_code_control', array(
    'label'    => __( 'Postal code', 'SKNGP SGGW' ),
    'section'  => 'address_section',
    'settings' => 'postal_code',
    'type'     => 'option'
  ) );

  $wp_customize->add_control( 'locality_control', array(
    'label'    => __( 'Postal code', 'SKNGP SGGW' ),
    'section'  => 'address_section',
    'settings' => 'locality',
    'type'     => 'option'
  ) );

  $wp_customize->add_control( 'address_control', array(
    'label'    => __( 'Address', 'SKNGP SGGW' ),
    'section'  => 'address_section',
    'settings' => 'address',
    'type'     => 'option'
  ) );

  $wp_customize->add_control( 'additional_info', array(
    'label'    => __( 'Additional information', 'SKNGP SGGW' ),
    'section'  => 'address_section',
    'settings' => 'additional_info',
    'type'     => 'option'
  ) );

  /*
   * Contact section
   */
  $wp_customize->add_section( 'contact_section', array(
    'title' => __( 'Contact section', 'SKNGP SGGW' ),
    'priority' => 35
  ) );

  $wp_customize->add_setting( 'main_phone', array(
    'default' => '(22) 59-35-680',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'additional_phone', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'main_email', array(
    'default' => 'skngp.sggw@gmail.com',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'visits_info', array(
    'default' => 'Budynek nr 34 pokój 0/55<br>
            Środa 9<sup>00</sup>-11<sup>00</sup><br>
            Wtorek 12<sup>00</sup>-14<sup>00</sup><br>',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'main_phone_control', array(
    'label' => __( 'Main phone', 'SKNGP SGGW' ),
    'section' => 'contact_section',
    'settings' => 'main_phone',
    'type' => 'option' 
  ) );

  $wp_customize->add_control( 'additional_phone_control', array(
    'label' => __( 'Additional phone', 'SKNGP SGGW' ),
    'section' => 'contact_section',
    'settings' => 'additional_phone',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'main_email_control', array(
    'label' => __( 'Main e-mail', 'SKNGP SGGW' ),
    'section' => 'contact_section',
    'settings' => 'main_email',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'visits_controller', array(
    'label' => __( 'Visits information', 'SKNGP SGGW' ),
    'section' => 'contact_section',
    'settings' => 'visits_info',
    'type' => 'option'
  ) );

  /*
   * Social links section
   */
  $wp_customize->add_section( 'social_links_section', array(
    'title' => __( 'Social links', 'SKNGP SGGW' ),
    'priority' => 40
  ) );

  $wp_customize->add_setting( 'facebook_link', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'gplus_link', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_setting( 'tweeter_link', array(
    'default' => '',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'facebook_link_control', array(
    'label' => __( 'Facebook profile', 'SKNGP SGGW' ),
    'section' => 'social_links_section',
    'settings' => 'facebook_link',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'gplus_link_control', array(
    'label' => __( 'Google+ profile', 'SKNGP SGGW' ),
    'section' => 'social_links_section',
    'settings' => 'gplus_link',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'tweeter_link_control', array(
    'label' => __( 'Tweeter profile', 'SKNGP SGGW' ),
    'section' => 'social_links_section',
    'settings' => 'tweeter_link',
    'type' => 'option'
  ) );

  /*
   * Active menu configuration
   */
  $wp_customize->add_section( 'url_section', array(
    'title' => __( 'URL Section', 'SKNGP SGGW' ),
    'priority' => 40
  ) );

  $wp_customize->add_setting( 'category_url_match', array(
    'default' => '/category/',
    'transport' => 'refresh',
    'capability' => 'edit_theme_options',
    'type' => 'option'
  ) );

  $wp_customize->add_control( 'category_url_match_control', array(
    'label' => __( 'Category RegEx for URL', 'SKNGP SGGW' ),
    'section' => 'url_section',
    'settings' => 'category_url_match',
    'type' => 'option'
  ) );
}

add_action( 'customize_register', 'skngp_setup_customization' );

function skngp_setup_theme() {
  add_theme_support( 'post-thumbnails' );

  add_image_size( 'tile-thumb', 380, 9999 );

  set_post_thumbnail_size( 380, 250, true );

  register_nav_menus( array(
    'top-menu' => __( 'Top menu', 'SKNGP SGGW' ),
    'footer-menu-left' => __(' Footer menu - left', 'SKNGP SGGW' ),
    'footer-menu-middle' => __(' Footer menu - middle', 'SKNGP SGGW' ),
    'footer-menu-right-top' => __(' Footer menu - right top', 'SKNGP SGGW' ),
    'footer-menu-right-bottom' => __(' Footer menu - right bottom', 'SKNGP SGGW' )
  ) );

  register_sidebar( array(
    'name' => __( 'Header area', 'SKNGP SGGW' ),
    'id' => 'header_area',
    'description' => __( 'Right aside of the global header', 'SKNGP SGGW' )
  ) );

  register_sidebar( array(
    'name' => __( 'Aqua area - Contact template', 'SKNGP SGGW' ),
    'id' => 'aqua_area_contact_template',
    'description' => __( 'Aqua area on the contact template', 'SKNGP SGGW' )
  ) );

  register_sidebar( array(
    'name' => __( 'White area - Contact template', 'SKNGP SGGW' ),
    'id' => 'white_area_contact_template',
    'description' => __( 'White area on the contact template', 'SKNGP SGGW' )
  ) );

  register_sidebar( array(
    'name' => __( 'Map area - Contact template', 'SKNGP SGGW' ),
    'id' => 'map_area_contact_template',
    'description' => __( 'Map area on the contact template', 'SKNGP SGGW' )
  ) );

  members_register_post_type();
  members_register_taxonomies();
}

add_action( 'init', 'skngp_setup_theme' );

function skngp_enqueue_scripts() {
  wp_enqueue_script( 'jquery' );
  wp_register_script( 'bootstrap', 'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js', array(), NULL, true );
  wp_enqueue_script( 'bootstrap' );

  wp_register_script( 'SKNGP SGGW', get_stylesheet_directory_uri().'/static/js/skngp.js', array('bootstrap' ), NULL, true );
  wp_enqueue_script( 'SKNGP SGGW' );
}

add_action( 'init', 'skngp_enqueue_scripts' );