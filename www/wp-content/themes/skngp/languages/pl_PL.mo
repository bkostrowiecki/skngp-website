��    "      ,  /   <      �     �          #     ?     X     o     �     �     �  !   �     �     �               $  
   0     ;      W     x     }     �  4   �     �  4   �     	     #     ,     <     O  "   m  X   �     �     �  �       �  !   �  '   �     �     	      	     9	     ?	  &   S	  .   z	     �	     �	     �	     �	     �	     
      
  @   >
     
     �
     �
  4   �
  	   �
  4   �
  N        n     z     �  "   �  P   �  X        k     �                                                                "   
                                                                       !       	              Footer menu - left  Footer menu - middle  Footer menu - right bottom  Footer menu - right top Additional information Additional phone Address Address informations Aqua area - Contact template Aqua area on the contact template Contact section Dyżury zarządu SKNGP Facebook profile Google+ profile Main e-mail Main phone Map area - Contact template Map area on the contact template Page Postal code Social links Studenckie Koło Naukowe<br>Gospodarki Przestrzennej Subtitle Szkoła Główna Gospodarstwa Wiejskiego w Warszawie There are no post to show Top menu Tweeter profile Visits information White area - Contact template White area on the contact template Wszelkie prawa zastrzeżone. Kopiowanie jakichkolwiek treści bez zezwolenia zabronione. Wydział Leśny SGGW d.m.Y Project-Id-Version: SKNGP SGGW
POT-Creation-Date: 2015-07-12 15:23+0100
PO-Revision-Date: 2015-07-12 22:08+0100
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.9
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: .
 Menu w stopce - lewa kolumna Menu w stopce - środkowa kolumna Menu w stopce - prawa kolumna (niższa) Menu w stopce - prawa kolumna Informacje dodatkowe Dodatkowy numer telefonu Adres Informacje adresowe Szablon kontaktowy - błękitny obszar Błękitny obszar na szablonie strony kontaktu Sekcja kontaktowa Dyżury zarządu SKNGP Profil Facebook Profil Google+ Głowny adres email Główny numer telefonu Szablon kontaktowy - Obszar mapy Obszar obok informacji kontaktowych, domyślnie miejsce na mapę Strona Kod pocztowy Linki społecznościowe Studenckie Koło Naukowe<br>Gospodarki Przestrzennej Podtytuł Szkoła Główna Gospodarstwa Wiejskiego w Warszawie Niestety, nie udało się znaleźć żadnych notek pasujacych do kryteriów :( Górne menu Profil Tweeter Informacje nt. biura Szablon kontaktowy - biały obszar Biały obszar na szablonie strony kontaktu, umieszczony pod obszarem błękitnym Wszelkie prawa zastrzeżone. Kopiowanie jakichkolwiek treści bez zezwolenia zabronione. Wydział Leśny SGGW d.m.Y 