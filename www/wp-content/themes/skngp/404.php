<?php 
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage SKNGP Theme
 * @since SKNGP Theme 1.0
 */

get_header(); ?>

  <main class="page-body">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        the_breadcrumb();
        ?>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h1>Strona nie istnieje!</h1>
        <p>Niestety, strona o podanym adresie nie istnieje</p>
      </div>
      <?php
      endwhile;
      endif;
      ?>
      </div>
    </div>
  </main>

<?php 
get_footer();
?>