module.exports = function (grunt) {
  require('jit-grunt')(grunt);

  // configure the tasks
  grunt.initConfig({
    less: {
      build: {
        options: {
          modifyVars: {
//            imgPath: '"http://mycdn.com/path/to/images"'
          },
          compress: true,
          yuicompress: true,
          optimization: 2
        },
        files: {
          "static/css/skngp.css": "static/css/style.less"
        }
      }
    },
    uglify: {
      build: {
        options: {
//          mangle: false
        },
        files: {
          'static/js/skngp.js': ['static/js/library.js', 'static/js/contact.js', 'static/js/bootstrap.js']
        }
      }
    },
    watch: {
      less: {
        files: 'static/**/*.less',
        tasks: [ 'less:build' ]
      },
      scripts: {
        files: ['static/js/bootstrap.js', 'static/js/contact.js', 'static/js/library.js'],
        tasks: [ 'uglify:build' ]
      }
    },
    copy: {
      dist: {
        cwd: '',
        src: [ 'static/**', '*.html' ],
        dest: 'dist/',
        expand: true
      }
    },
    clean: {
      dist: {
        src: [
          "dist/static/css/*.less",
          "!dist/static/css/*.css",
          "dist/static/components/**/grunt",
          "dist/static/components/**/bower.*",
          "dist/static/components/**/Gruntfile.*",
          "dist/static/components/**/package*",
          "dist/static/components/less"
        ]
      }
    }
  });

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-less', 'grunt-contrib-uglify', 'grunt-contrib-copy', 'grunt-contrib-clean');

  // define the tasks
  grunt.registerTask('build', ['less:build', 'uglify:build']);

  grunt.registerTask('dist', ['copy:dist', 'clean:dist']);

  grunt.registerTask('default', 'Watches the project for changes, automatically builds them.', [ 'build', 'watch' ]);
};