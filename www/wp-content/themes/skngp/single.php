<?php

get_header();

?>
  <main class="page-body">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <?php
        the_breadcrumb();
        ?>
      </div>
      <?php
        if ( have_posts() ) : while ( have_posts() ) : the_post();
        $postID = get_the_ID();

        $thumbnailUrl = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        ?>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <article id="<?php the_ID(); ?>" class="article article--single <?php the_post_class_string( $postID ); ?>">
          <?php if ( has_post_thumbnail() ): ?>
          <div class="article__thumbnail">
          <?php
            the_post_thumbnail( 'tile-thumb' );
          ?>
          </div>
          <?php endif; ?>
          <div class="article__heading"><?php the_title(); ?></div>
          <time datetime="<?php echo get_the_date(); ?>" class="article__publish-date"><?php echo get_the_date(__('d.m.Y')); ?></time>
          <div class="article__content">
            <?php the_content(); ?>
          </div>
        </article>
      </div>
      <?php
    endwhile;
    else:
      ?><h2><?php _e( 'There are no post to show', 'SKNGP SGGW' ); ?></h2><?php
    endif;
    ?>
    </div>
  </main>

<?php

get_footer();

?>