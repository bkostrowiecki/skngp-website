<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>" />
  <meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <link rel="apple-touch-icon" href="favicon.png" />

  <link href="http://fonts.googleapis.com/css?family=Roboto:500,300,500italic,300italic&subset=latin,latin-ext" rel="stylesheet" type="text/css" />
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,700italic,300,700&subset=latin,latin-ext" rel="stylesheet" type="text/css" />
  <link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300&subset=latin,latin-ext" rel="stylesheet" type="text/css" />
  
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" />
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/static/css/skngp.css" media="all" />

  <link rel="profile" href="http://microformats.org/profile/hcard" />

  <!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
  </script>
  <![endif]-->
  
  <title><?php wp_title( '|', true, 'right' ); bloginfo( 'name' ); ?></title>
</head>
<body>
  <div class="page-head container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <a class="brand link link--brand" href="/" title="Idź do strony startowej">
          <div class="brand__symbol">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/static/img/logo_symbol.png" alt="Logotyp SKNGP" class="img-responsive">
          </div>
          <?php if ( is_home() || is_front_page() ) : ?>
          <h1 class="brand__logotype">
            <?php bloginfo( 'name' ); ?>
            <div class="brand__logotype__sub"><?php the_option( 'subtitle' ); ?></div>
          </h1>
          <?php else: ?>
          <div class="brand__logotype">
            <?php bloginfo( 'name' ); ?>
            <div class="brand__logotype__sub"><?php the_option( 'subtitle' ); ?></div>
          </div>
          <?php endif; ?>
        </a>
        <ul class="list list--unstyled">
          <?php dynamic_sidebar( 'header_area' ); ?>
        </ul>
      </div>
    </div>
    <div class="header-decoration"></div>
  </div>
  <div class="page-neck">
    <div class="navbar-header navbar-header--main">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#topNavbarCollapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="topNavbarCollapse">
    <div class="container">
      <div class="row">
        <nav class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?php skngp_top_menu( 'top-menu' ); ?>
        </nav>
      </div>
    </div>
    </div>
  </div>
