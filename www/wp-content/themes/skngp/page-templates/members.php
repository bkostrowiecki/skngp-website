<?php

/*
Template Name: Members Page Template
*/

get_header();

?>
  <main class="page-body">
    <div class="container">
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <?php while ( have_posts() ) : the_post() ?>
          <h1 class="page-heading"><?php the_title(); ?></h1>
          <article class="article">
            <?php
            the_content();
            ?>
          </article>
          <?php endwhile; ?>
          <section class="section section--members">
          <?php $managmentLoop = new WP_Query( array(
              'post_type' => 'members',
              'members-status' => 'zarzad'
            ) );
            $num = 0;
            while ( $managmentLoop->have_posts() ):
            $managmentLoop->the_post();
            if ( $num % 2 === 0 ) :
            ?>
            <div class="row">
            <?php
            endif;
            ?>
              <article class="article article--member col-lg-6 col-md-6 col-sm-12 col-xs-12 col-uxs-12">
                <figure class="article__figure">
                  <?php the_post_thumbnail( 'member-thumb' ); ?>
                </figure>
                <h2 class="article__heading"><?php the_title(); ?></h2>
                <div class="article__subheading"><?php $terms =  get_the_terms( get_the_ID(), 'members-status' ); foreach ( $terms as $term ) { echo $term->name; break; } ?></div>
                <div class="article__content"><?php the_content(); ?></div>
              </article>
            <?php if ( $num % 2 === 1 ) : ?>
            </div>
            <?php endif; 
            $num++; ?>
          <?php endwhile;?>
          <?php if ( $num % 2 === 0 ) : ?>
            </div>
          <?php endif; ?>
          </section>
       </div>
    </div>
    <div class="bg bg--aqua">
      <section class="container">
      <?php $managmentLoop = new WP_Query( array(
        'post_type' => 'members',
        'members-status' => 'opiekun'
        ) );
      while ( $managmentLoop->have_posts() ):
        $managmentLoop->the_post();
      ?>
        <article class="article article--member col-lg-12 col-md-12 col-sm-12 col-xs-12 col-uxs-12">
          <figure class="article__figure">
            <?php the_post_thumbnail( 'member-thumb' ); ?>
          </figure>
          <h2 class="article__heading"><?php the_title(); ?></h2>
          <div class="article__subheading"><?php $terms =  get_the_terms( get_the_ID(), 'members-status' ); foreach ( $terms as $term ) { echo $term->name; break; } ?></div>
          <div class="article__content"><?php the_content(); ?></div>
        </article>
        <?php endwhile;?>
      </div>
    </div>
    <div class="container bg">
      <div class="col-lg-push-1 col-md-push-1 col-lg-9 col-md-8 col-sm-8 col-xs-12 pull-right">
        <img src="static/img/content/map.png" alt="" class="img-responsive">
      </div>
      <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 pull-left">
        <h3 class="page-heading">Członkowie koła</h3>
        <ul class="list list--unstyled">
        <?php $membersLoop = new WP_Query( array(
          'post_type' => 'members',
          'members-status' => 'czlonek',
          'posts_per_page' => -1,
          'orderby' => 'title',
          'order' => 'ASC'
        ) );
        while ( $membersLoop->have_posts() ):
          $membersLoop->the_post(); ?>
          <li class="list__item"><?php the_title(); ?></li>
        <?php endwhile; ?>
        </ul>
      </div>
    </div>
  </main>

  <?php

  get_footer();

  ?>