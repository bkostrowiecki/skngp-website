<?php

/*
Template Name: Categories Page
*/

get_header();

?>
  <main class="page-body">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-uxs-12">  
        <ul class="list list--unstyled list--categories">
          <?php 
            if ( function_exists( 'ryju_caticons_list' ) ) {
              ryju_caticons_list(''); 
            }
          ?>
        </ul>
      </div>
    <?php  
      if ( have_posts() ) : while ( have_posts() ) : the_post();
      $postID = get_the_ID();
      ?>
      <article id="<?php the_ID(); ?>" class="col-lg-4 col-md-4 col-sm-6 col-xs-6 col-uxs-12 tile-col <?php the_post_class_string( $postID ); ?>">
        <div class="tile">
          <?php if ( has_post_thumbnail() ): 
            the_post_thumbnail( 'tile-thumb' );
          else: ?>
          <img src="<?php echo get_stylesheet_directory_uri().'/static/img/single-placeholder.jpg'; ?>" />
          <?php endif; ?>
          <div class="tile__content">
            <div class="tile__content__heading">
              <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="link link--tile-heading"><?php the_title(); ?></a>
            </div>
            <div class="tile__content__preview"><?php the_content(); ?></div>
            <time datetime="<?php echo get_the_date(); ?>" class="tile__content__publish-date"><?php echo get_the_date(__('d.m.Y')); ?></time>
          </div>
        </div>
      </article>
    <?php
    endwhile;
    endif;
    ?>
    </div>
  </main>

<?php

get_footer();

?>