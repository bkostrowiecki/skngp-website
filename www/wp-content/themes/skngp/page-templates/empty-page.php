<?php

/*
Template Name: Empty Page
*/

get_header();

?>
  <main class="page-body">
    <div class="container">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <?php
      the_breadcrumb();

      if ( have_posts() ) : while ( have_posts() ) : the_post();
      $postID = get_the_ID();
      ?>
      </div>
      <div class="col-lg-12 col-md-12- col-sm-12 col-xs-12">
      <article id="<?php the_ID(); ?>" class="article article--single <?php the_post_class_string( $postID ); ?>">
        <div class="article__content">
          <?php the_content(); ?>
        </div>
      </article>
      </div>
      <?php
      endwhile;
      endif;
      ?>
    </div>
  </main>

<?php

get_footer();

?>