<?php

/*
Template Name: Contact Page
*/

get_header();

?>
  <div class="page-body">
    <div class="container section">
      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 vcard" id="contactInfo">
        <h1 class="page-heading fn org"><?php _e( 'Studenckie Koło Naukowe<br>Gospodarki Przestrzennej' ); ?></h1>
        <div class="contact-info">
          <div class="info info--contact">
            <div class="info__icon">
              <i class="icon icon--homeadress_b"></i>
            </div>
            <div class="info__content adr">
              <span class="postal-code"><?php the_option( 'postal_code' ); ?></span> <span class="locality"><?php the_option( 'locality' ); ?></span><br>
              <span class="street-address">
                <?php the_option( 'address' ); ?><br>
                <?php the_option( 'additional_info' ); ?>
              </span>
            </div>
          </div>
          <div class="info info--contact">
            <div class="info__icon">
              <i class="icon icon--phone_b"></i>
            </div>
            <div class="info__content info__content--one-line">
              <span class="tel"><?php the_option( 'main_phone' ); ?></span>
            </div>
          </div>
          <div class="info info--contact">
            <div class="info__icon">
              <i class="icon icon--adress_b"></i>
            </div>
            <div class="info__content info__content--one-line">
              <span class="email"><?php the_option( 'main_email' ); ?></span>
            </div>
          </div>
        </div>
        <h2 class="page-heading"><?php _e( 'Dyżury zarządu SKNGP' ); ?></h2>
        <div class="info info--contact">
          <div class="info__icon">
            <i class="icon icon--watch_b"></i>
          </div>
          <div class="info__content">
            <?php the_option( 'visits_info' ); ?>
          </div>
        </div>
      </div>
      <div class="col-lg-7 col md-7 col-sm-7 col-xs-12">
        <ul class="list list--unstyled">
        <?php dynamic_sidebar( 'map_area_contact_template' ); ?>
        </ul>
      </div>
    </div>
    <div class="bg--aqua" id="AquaWidgetArea">
      <div class="container">
        <div class="row">
          <ul class="list list--unstyled">
            <?php dynamic_sidebar( 'aqua_area_contact_template' ); ?>
          </ul>
        </div>
      </div>
    </div>
    <div class="container quick-form" id="WhiteWidgetArea">
      <ul class="list list--unstyled">
        <?php dynamic_sidebar( 'white_area_contact_template' ); ?>
      </ul>
    </div>
  </div>
<?php

get_footer();

?>