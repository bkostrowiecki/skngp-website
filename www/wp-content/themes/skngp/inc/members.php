<?php

function members_register_taxonomies() {
  register_taxonomy( 'members-status', 'members',
    array(
      'hierarchical' => true,
      'labels' => array(
        'name' => 'Status',
        'singular_name' => 'Status',
        'search_items' => 'Przeszukaj statusy',
        'popular_items' => 'Popularne statusy',
        'add_new_items' => 'Dodaj nowy status'
      ),
      'query_var' => true,
      'rewrite' => array( 'slug' => 'status' )
    )
  );
}

function members_register_post_type() {
  add_image_size( 'member-thumb', 145, 190 );
  
  register_post_type('members', array(
    'labels' => array(
      'name' => 'Członkowie',
      'menu_name' => 'Członkowie'
    ),
    'sinular_label' => 'Członek',
    'public' => true,
    'show_ui' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'hierarchical' => false,
    'show_ui_menu' => true,
    'supports' => array( 'title', 'editor', 'author', 'revisions', 'comments', 'thumbnail' )
    )
  );
}

?>