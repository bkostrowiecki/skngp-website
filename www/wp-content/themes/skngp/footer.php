<div class="page-leg">
    <div class="container relative">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-uxs-12 footer-col-margin">
          <div class="info vcard" id="footerContactInfo">
            <span class="fn org hidden">Studenckie koło naukowe gospodarki przestrzennej <abbr title="Szkoła główna gospodarstwa wiejskiego">SGGW</abbr></span>
            <div class="info__icon">
              <div class="address-icon"></div>
            </div>
            <div class="info__content">
              <span class="adr">
              <span class="postal-code"><?php the_option( 'postal-code' ); ?></span> <span class="locality"><?php the_option( 'locality '); ?></span><br>
              <span class="address"><?php the_option( 'address' ); ?><br>
              <?php the_option( 'additional_info' ); ?><br></span>
              </span>
            </div>
          </div>
          <div class="info">
            <div class="info__icon">
              <div class="phone-icon"></div>
            </div>
            <div class="info__content">
              <span class="tel"><?php the_option( 'main_phone' ); ?></span><br>
              <span class="tel"><?php the_option( 'additional_phone' ); ?></span><br>
            </div>
          </div>
          <div class="info">
            <div class="info__icon">
              <div class="mail-icon"></div>
            </div>
            <div class="info__content info__content--one-line">
              <a class="link link--footer email" href="mailto:<?php the_option( 'main_email' ); ?>"><?php the_option( 'main_email' ); ?></a><br>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col-uxs-12 footer-col-margin">
          <?php if ( get_option( 'facebook_link' ) ) : ?>
          <div class="info">
            <div class="info__icon">
              <div class="fb-icon"></div>
            </div>
            <div class="info__content info__content--one-line">
              <a href="http://<?php the_option( 'facebook_link' ); ?>" class="link link--footer"><?php the_option( 'facebook_link' ); ?></a>
            </div>
          </div>
          <?php endif;
          if ( get_option( 'gplus_link' ) != '' ) : ?>
          <div class="info">
            <div class="info__icon">
              <div class="gplus-icon"></div>
            </div>
            <div class="info__content info__content--one-line">
              <a href="http://<?php the_option( 'gplus_link' ); ?>" class="link link--footer"><?php the_option( 'gplus_link' ); ?></a>
            </div>
          </div>
          <?php endif;
          if ( get_option( 'tweeter_link' ) != '' ) : ?>
          <div class="info">
            <div class="info__icon">
              <div class="tweeter-icon"></div>
            </div>
            <div class="info__content info__content--one-line">
              <a href="tweeter.com/<?php the_option( 'tweeter_link' ); ?>" class="link link--footer"><?php the_option( 'tweeter_link' ); ?></a>
            </div>
          </div>
          <?php endif; ?>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-10 col-xs-12 sitemap footer-col-margin">
          <div>
            <nav class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-uxs-12">
              <?php skngp_footer_menu( 'footer-menu-left' ); ?>
            </nav>
            <nav class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-uxs-12">
              <?php skngp_footer_menu( 'footer-menu-middle' ); ?>
            </nav>
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-4 col-uxs-12">
              <nav>
                <?php skngp_footer_menu( 'footer-menu-right-top' ); ?>
              </nav>
              <nav>
                <?php skngp_footer_menu( 'footer-menu-right-bottom' ); ?>
              </nav>
            </div>
          </div>
          <div class="footer-symbol"></div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-2 col-xs-12">
          <a class="link link--wl-logo col-xs-12 pull-right" href="http://wl.sggw.pl"><?php _e( 'Wydział Leśny SGGW', 'SKNGP SGGW' ); ?></a>
          <a class="link link--sggw-logo col-xs-12 pull-right" href="http://www.sggw.pl"><?php _e( 'Szkoła Główna Gospodarstwa Wiejskiego w Warszawie', 'SKNGP SGGW' ); ?></a>
        </div>
      </div>
      <div class="footer-decoration"></div>
    </div>
  </div>

  <div class="page-foot text-center">
    Copyright © <?php echo date( 'Y' ); ?> SKNGP. <?php _e( 'Wszelkie prawa zastrzeżone. Kopiowanie jakichkolwiek treści bez zezwolenia zabronione.' ); ?>
  </div>

  <?php wp_footer(); ?>
</body>
</html>