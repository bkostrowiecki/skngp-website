��          �      �           	                9     I  +   W  -   �  *   �     �  G   �     2     :     H     V  F   l     �     �     �  	   �     �     �  ;     �  R     �     �                 A  -   W  @   �  ,   �     �  i        l     x     �     �  O   �     �     	  #   	     9	     @	     S	  I   o	                                                	                                       
                 Email Enter your email Enter your message here  Enter your name Fail template Fill in a message field, it cannot be empty Fill in a signature field, it cannot be empty Fill in an email field, it cannot be empty Help template I am pretty sure that you did not type correct name. Please correct it. Message Question from Quick message Ryju Quickform Widget Ryju qucikform widget, allow you to put a qucikform into a widget area Save changes Send Settings have been updated Signature Success template Type correct email address You do not have sufficient permissions to access this page. Project-Id-Version: Ryju AJAX Quickform
POT-Creation-Date: 2015-07-12 17:32+0100
PO-Revision-Date: 2015-07-12 17:40+0100
Last-Translator: 
Language-Team: 
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.9
X-Poedit-Basepath: ..
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SearchPath-0: .
 Email Wpisz tutaj swój adres email Wpisz swoją wiadomość tutaj Wpisz swoje imię i/lub nazwisko Szablon niepowodzenia Pole wiadomości jest wymagane. Wypełnij je. Pole sygnatury jest wymagane. Wpisz swoję imię i/lub nazwisko. Pole email jest wymagane. Wpisz adres email. Szablon pomocy Jestem prawie pewny, że to nie jest Twoje prawdziwe imię. Proszę, abyś wpisał swoje prawdziwe imię. Wiadomość Pytanie od  Szybka wiadomość Ryju Quick Contact Form Umożliwia umieszczenie formularza szybkiego kontaktu w obszarzenia widżetów. Zapisz zmiany Wyślij Ustawienia zostały zaktualizowane. Podpis Szablon powodzenia Wpisz poprawny adres email. Nie masz wystarczających uprawnień, aby uzyskać dostęp do tej strony. 