<?php

class Ryju_QuickformWidget extends WP_Widget {
  function __construct() {
    parent::__construct( 
      'ryju_quickform_widget',
      __( 'Ryju Quickform Widget', 'ryju-quickform-plugin' ),
      array(
        'description' => __( 'Ryju qucikform widget, allow you to put a qucikform into a widget area', 'ryju-quickform-plugin' ),
      )
    );
  }

  public function widget( $args, $instance ) {
    echo $args[ 'before_widget' ];
    echo do_shortcode( '[ryju_quickform /]' );
    echo $args[ 'after_widget' ];
  }

  public function form( $instance ) {
  }

  public function update( $new_instance, $old_instance ) {
    return $instance;
  } 
};

function ryju_quickform_register_widget() {
  register_widget( 'Ryju_QuickformWidget' );
}

add_action( 'widgets_init', 'ryju_quickform_register_widget' );