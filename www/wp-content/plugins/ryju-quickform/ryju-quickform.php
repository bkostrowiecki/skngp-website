<?php

/**
* Plugin Name: Ryju AJAX Quickform
* Plugin URI: no URI
* Description: That plugin creates the AJAX quick contact form.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
* Text Domain: ryju-quickform-plugin
*/

function ryju_quickform_setup_language() {
  if ( !load_plugin_textdomain( 'ryju-quickform-plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' )) {
    echo '<br>No translation';
  }
}

add_action( 'plugins_loaded', 'ryju_quickform_setup_language' );

function ryju_quickform_setup() {
  wp_enqueue_script( 'jquery' );

  wp_enqueue_script( 'ryju-quickform-plugin-script', plugins_url( '/js/ryju-quickform.js', __FILE__ ), array( 'jquery' ), false, true );

  $admin_ajax_url = admin_url( 'admin-ajax.php', 'http://' );
  wp_localize_script( 'ryju-quickform-plugin-script', 'ajax_options', array( 'url' => $admin_ajax_url ) );
}

function ryju_quickform_send() {
  ob_clean();

  $signature = $_POST[ 'signature' ];
  $email = $_POST[ 'email' ];
  $message =  $_POST[ 'message' ]."\n\n".$signature.' <'.$email.'>';

  $response = array();

  if ( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
    $response['message_email'] = __( 'Type correct email address', 'ryju-quickform-plugin' );
    $response['status'] = 'not_valid';
  }
  if ( $email == '' ) {
    $response['message_email'] = __( 'Fill in an email field, it cannot be empty', 'ryju-quickform-plugin' );
    $response['status'] = 'not_valid';
  }

  if ( !preg_match( "/^[a-zA-Z ]*$/", $signature ) ) { 
    $response['message_signature'] = __( 'I am pretty sure that you did not type correct name. Please correct it.', 'ryju-quickform-plugin' );
    $response['status'] = 'not_valid';
  }
  if ( $signature == '' ) {
    $response['message_signature'] = __( 'Fill in a signature field, it cannot be empty', 'ryju-quickform-plugin' );
    $response['status'] = 'not_valid';
  }

  if ( $_POST[ 'message' ] == '' ) {
    $response['message_message'] = __( 'Fill in a message field, it cannot be empty', 'ryju-quickform-plugin' );
    $response['status'] = 'not_valid';
  }

  if ( $response['status'] === 'not_valid' ) {
    echo json_encode( $response );
    wp_die();
  }

  $subject = __( 'Question from', 'ryju_quckform_plugin' ).$signature.' <'.$email.'>';

  if ( !wp_mail( $to, $subject, $message ) ) {
    $response['status'] = 'error';
    $response['message'] = get_option( 'sending_failed_template' );
    echo json_encode( $response );
    wp_die();
  }

  $response['status'] = 'success';
  $response['message'] = get_option( 'sending_success_template');
  echo json_encode( $response );

  wp_die();
}

add_action( 'init', 'ryju_quickform_setup' );

add_action( 'wp_ajax_send_quickform', 'ryju_quickform_send' );
add_action( 'wp_ajax_nopriv_send_quickform', 'ryju_quickform_send' ); 

function ryju_quickform_shortcode_func() {
  $text = '<div id="ryju-quickform-box">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 breath-30" id="ryju-quickform-info">';

  if ( get_option( 'help-template' ) !== false && get_option( 'help_template' ) !== '') {
  $text .= '<h2 class="jumbo-heading">'.__( 'Quick message', 'ryju-quickform-plugin' ).'</h2>
      <div class="jumbo-message jumbo-message--orange">
        <i class="icon icon--excl-orange pull-left"></i>
        '.get_option( 'help_template' ).'
      </div>';
  }
  $text .= '
  </div>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 hidden" id="ryju-quickform-connection-error">
    <h2>Błąd z połączeniem</h2>
    <div class="jsErrorMessage">
    '.get_option( 'sending_failed_template' ).'
    </div>
  </div>
  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="ryju-quickform-content">
    <form action="#" class="form-horizontal" id="ryju-quickform">
      <div class="form-group" id="messageGroup">
        <label for="input1" class="col-lg-3 col-md-3 col-sm-2 col-xs-12 control-label control-label--contact control-label--textarea">'.__( 'Message', 'ryju-quickform-plugin' ).'</label>
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 form-control-wraper">
          <i class="icon peck-large pull-left hidden-xs"></i>
          <textarea id="input1" name="message" placeholder="'.__( 'Enter your message here ', 'ryju-quickform-plugin' ).'" class="form-control form-control--contact form-control--textarea"></textarea>
          <span class="help-block"></span>
        </div>
      </div>
      <div class="form-group" id="emailGroup">
        <label for="input2" class="col-lg-3 col-md-3 col-sm-2 col-xs-12 control-label control-label--contact">'.__( 'Email', 'ryju-quickform-plugin' ).'</label>
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 form-control-wraper">
          <i class="icon peck pull-left hidden-xs"></i>
          <input type="email" name="email" id="input2" placeholder="'.__( 'Enter your email', 'ryju-quickform-plugin' ).'" class="form-control form-control--contact">
          <span class="help-block"></span>
        </div>
      </div>
      <div class="form-group" id="signatureGroup">
        <label for="input3" class="col-lg-3 col-md-3 col-sm-2 col-xs-12 control-label control-label--contact">'.__( 'Signature', 'ryju-quickform-plugin' ).'</label>
        <div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 form-control-wraper">
          <i class="icon peck pull-left hidden-xs"></i>
          <input type="text" name="signature" id="input3" placeholder="'.__( 'Enter your name', 'ryju-quickform-plugin' ).'" class="form-control form-control--contact">
          <span class="help-block"></span>
        </div>
      </div>
      <button type="submit" class="btn btn--default pull-right">'.__( 'Send', 'ryju-quickform-plugin' ).'</button>
    </form>
  </div>
  <div class="col-lg-push-1 col-md-push-1 col-lg-3 col-md-3 hidden-sm hidden-xs">
    <i class="icon icon--form"></i>
  </div>
  </div>';

  return do_shortcode( $text );
}

add_shortcode( 'ryju_quickform', 'ryju_quickform_shortcode_func' );

/**
 * Function that gets options from the admin and save it as options to a database
 * 
 * If there is no "update_settings" sent by POST, then it only renders a form.
 *
 * @since ryju-smtp-config 1.0.0
 * 
 * @return void
 */
function ryju_quickform_options() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

  if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  // Updates options in Wordpress if update_settings is recived from POST
  if (isset($action)) {
    if ($action==='update') {
      $sending_success_template = esc_attr($_POST['sending_success_template']);   
      update_option('sending_success_template', $sending_success_template);
      $sending_failed_template = esc_attr($_POST['sending_failed_template']);   
      update_option('sending_failed_template', $sending_failed_template);
      $help_template = esc_attr($_POST['help_template']);
      update_option('help_template', $help_template);
      echo '<h3>'.__('Settings have been updated').'</h3>';
    }
  }

  // Render the smtpconfig's options form
  ?>

  <h1>Ryju Quickform</h1>
  <form class="form" method="post" action="?page=ryju_quickform_options">
    <div class="form-group">
      <label for="sending_success_template"><?php _e( 'Success template', 'ryju-quickform-plugin' ) ?></label>
      <div>
        <textarea name="sending_success_template" id="sending_success_template" cols="80" rows="10"><?php echo get_option( 'sending_success_template' ); ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="sending_failed_template"><?php _e( 'Fail template', 'ryju-quickform-plugin' ) ?></label>
      <div>
        <textarea name="sending_failed_template" id="sending_failed_template" cols="80" rows="10"><?php echo get_option( 'sending_failed_template' ); ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="help_template"><?php _e( 'Help template', 'ryju-quickform-plugin' ) ?></label>
      <div>
        <textarea name="help_template" id="help_template" cols="80" rows="10"><?php echo get_option( 'help_template' ); ?></textarea>
      </div>
    </div>
    <input type="hidden" name="action" value="update">
    <button type="submit" class="button button-primary"><?php _e( 'Save changes', 'ryju-quickform-plugin' ); ?></button>
  </form>
  <?php
}

function ryju_quickform_admin_menu() {
  // Add options page
  add_options_page( 'ryju-quickform Settings', 'Ryju Quickform', 'manage_options', 'ryju_quickform_options', 'ryju_quickform_options' );
}
// Adds smtpconfig initialization's function while admin_menu initialization
add_action( 'admin_menu', 'ryju_quickform_admin_menu' );

require_once( 'ryju-quickform-widget.php' );