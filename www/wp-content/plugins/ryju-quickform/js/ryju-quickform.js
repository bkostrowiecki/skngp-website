var Quickform = (function ($) {
  var $form = $('form#ryju-quickform');

  $form.find('button[type=submit]').on('click', function(event) {
    event.preventDefault();
    postForm();
  });

  $form.find('input').on('change paste keyup', function () {
    $($(this).parents('.form-group').get(0)).removeClass('form-group--error');
  });

  function success(response) {
    $form.find('input').prop('disabled', false);
    $form.find('.form-group').removeClass('form-group--error');
    $form.find('.help-block').html('');
    response = JSON.parse(response);
    if (response.status === 'not_valid') {
      if (response.message_email !== undefined) {
        var $emailGroup = $form.find('#emailGroup');
        $emailGroup.addClass('form-group--error');
        $emailGroup.find('.help-block').html(response.message_email);
      }
      if (response.message_signature !== undefined) {
        var $signatureGroup = $form.find('#signatureGroup');
        $signatureGroup.addClass('form-group--error');
        $signatureGroup.find('.help-block').html(response.message_signature);
      }
      if (response.message_message !== undefined) {
        var $messageGroup = $form.find('#messageGroup');
        $messageGroup.addClass('form-group--error');
        $messageGroup.find('.help-block').html(response.message_message);
      }
      return;
    }
    else if (response.status === 'success') {
      $('#ryju-quickform-content').html($(response.message));
      $form.remove();
      $('#ryju-quickform-info').addClass('hidden');
    } else {
      if (response.message !== false) {
        error(response.message);
      }
    }
  }

  function error(error) {
    $('#ryju-quickform-connection-error').css('display', 'none').removeClass('hidden').fadeIn(500);
    $('#ryju-quickform-info').addClass('hidden');
    $('#ryju-quickform-content').addClass('hidden');
    $form.addClass('hidden');
  }

  function postForm() {
    $form.find('input').prop('disabled', true);

    var data = {
      message: $form.find('#input1').val(),
      email: $form.find('#input2').val(),
      signature: $form.find('#input3').val(),
      action: 'send_quickform'
    };

    $.ajax({
      type: 'POST',
      url: ajax_options.url,
      data: data,
      success: success,
      error: error
    });
  }
}(jQuery.noConflict()));
