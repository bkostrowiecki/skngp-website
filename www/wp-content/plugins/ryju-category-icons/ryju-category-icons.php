<?php

/**
* Plugin Name: Ryju Category Icons
* Plugin URI: no URI
* Description: That plugin allows you to set an icon for a category. You need to use its special functions to render categories list.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
* Text Domain: ryju-caticons-plugin
*/

function ryju_caticons_setup_language() {
  if ( !load_plugin_textdomain( 'ryju-caticons-plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' )) {
    echo '<br>No translation';
  }
}

add_action( 'plugins_loaded', 'ryju_caticons_setup_language' );

function ryju_caticons_list( $parent ) {
  $categories = get_categories( array(
    'type'                     => 'post',
    'child_of'                 => 0,
    'parent'                   => $parent,
    'orderby'                  => 'name',
    'order'                    => 'ASC',
    'hide_empty'               => 1,
    'hierarchical'             => 1,
    'exclude'                  => '',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false,
  ));
  ?>

  <ul class="list list--unstyled list--categories">
  <?php
  foreach ( $categories as $category ) : 
    if (is_category() ) {
      $catID = get_query_var( 'cat' );
      $currentCat = get_category( $catID );
      $icon = get_option( 'ryju_category_icon_'.$category->cat_ID );
      if ( $icon === false ) {
        $icon = '';
      }
    }
    if ( $category->slug === 'bez-kategorii' ) {
      continue;
    }
    $isActive = ($currentCat->slug == $category->slug);
    ?>
      <li class="list--categories__item">
        <a href="<?php echo esc_url( get_category_link( $category->cat_ID ) ); ?>" class="link link--category link--category--<?php echo $category->slug; ?> <?php echo ($isActive) ? ' link--active' : ''?>">
          <i class="icon <?php echo $icon; ?> icon--category"></i><?php echo $category->name; ?>
        </a>
      </li>
  <?php
  endforeach;
  ?>
  </ul>
  <?php
}

function ryju_caticons_options() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.', 'ryju-caticons-plugin' ) );
  }

  if ( isset( $_POST['action'] ) ) {
    $action = htmlspecialchars($_POST['action']);
  }

  // Updates options in Wordpress if update_settings is recived from POST
  if ( isset( $action ) ) {
    if ( $action==='update' ) {
      foreach ( $_POST as $postname => $postvalue) {
        if ( substr( $postname, 0, strlen( 'ryju_category_icon_' ) ) === 'ryju_category_icon_' ) {
          update_option( $postname, $postvalue );
        }
      }
      echo '<h3>'.__('Settings have been updated', 'ryju-caticons-plugin' ).'</h3>';
    }
  }

  $categories = get_categories( array(
    'type'                     => 'post',
    'child_of'                 => 0,
    'parent'                   => $parent,
    'orderby'                  => 'name',
    'order'                    => 'ASC',
    'hide_empty'               => 1,
    'hierarchical'             => 1,
    'exclude'                  => '',
    'include'                  => '',
    'number'                   => '',
    'taxonomy'                 => 'category',
    'pad_counts'               => false,
  ));

  // Render the smtpconfig's options form
  ?>

  <h1><?php _e( 'Ryju Category Icons', 'ryju-caticons-plugin' ); ?></h1>
  <form class="form" method="post" action="?page=ryju_caticons_options">
    <?php foreach ( $categories as $cat ):
      $name = 'ryju_category_icon_'.$cat->cat_ID;
      $value = get_option( $name );
      if ( $value === false ) {
        $value = '';
      }
    ?>
    <div class="form-group">
      <label style="width: 250px; float: left;" for="<?php echo $name; ?>"><?php echo $cat->name.'('.$cat->slug.')'; ?></label>
      <input type="text" class="form-control" name="<?php echo $name; ?>" id="<?php echo $name; ?>" value="<?php echo $value; ?>">
    </div>
    <?php endforeach; ?>
    <input type="hidden" name="action" value="update">
    <button type="submit" class="button button-primary"><?php _e( 'Save changes', 'ryju-caticons-plugin' ); ?></button>
  </form>
  <?php
}

function ryju_caticons_admin_setup() {
  add_options_page( 'Ryju category icons settings', 'Ryju Caticons', 'manage_options', 'ryju_caticons_options', 'ryju_caticons_options' );
}

add_action( 'admin_menu', 'ryju_caticons_admin_setup' );