<?php

/**
* Plugin Name: Ryju SMTP Configuration
* Plugin URI: no URI
* Description: That plugin gives you a possibility to change Wordpress configuration for sending emails.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
*/

/**
 * Send the email to a user using wp_mail
 *
 * @since ryju-smtpc-onfig 1.0.0
 * 
 * @param  string  $title      Title of the email
 * @param  string  $content    Content of the email
 * @param  string  $toWho      Email address that will get the message
 * @param  boolean/string $attachment False, if there is no attachment, true if there is some
 * @return boolean             Returns true, if sending is successful, otherway returns false
 */
function ryju_smtpconfig_send_email($title, $content, $toWho, $attachment = false) {
  if (!$attachment) {
    $result = wp_mail($toWho, $title, $content);
  } else {
    // Header's setting is needed because of attachment
    $headers = 'From: '.get_option('smtpconfig_fromname').' <'.get_option('smtpconfig_from').'>'."\r\n";
    $result = wp_mail($toWho, $title, $content, array(), array($attachment));
  }
  return $result;
}

/**
 * Send the test email to the currently logged in user
 *
 * @since ryju-smtpc-onfig 1.0.0
 * 
 * @return boolean Returns true if sending message was successful, otherway false
 */
function ryju_smtpconfig_send_test() {
  global $current_user;

  get_currentuserinfo();
  $toWho = $current_user->user_email;

  if (!empty($toWho)) {
    return ryju_smtpconfig_send_email('Wiadomość testowa od '.get_bloginfo('name'), 'To jest wiadomość testowa od '.get_bloginfo('name').'. Konfiguracja SMTP jest poprawna.', $toWho);
  }
  return false;
}

/**
 * Function that gets options from the admin and save it as options to a database
 * 
 * If there is no "update_settings" sent by POST, then it only renders a form.
 *
 * @since ryju-smtp-config 1.0.0
 * 
 * @return void
 */
function ryju_smtpconfig_options() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

  if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  // Updates options in Wordpress if update_settings is recived from POST
  if (isset($action)) {
    if ($action==='update') {
      $smtpconfig_host = esc_attr($_POST["smtpconfig_host"]);   
      update_option("smtpconfig_host", $smtpconfig_host);
  
      $smtpconfig_smtpauth = esc_attr($_POST["smtpconfig_smtpauth"]);   
      update_option("smtpconfig_smtpauth", $smtpconfig_smtpauth);
  
      $smtpconfig_port = esc_attr($_POST["smtpconfig_port"]);   
      update_option("smtpconfig_port", $smtpconfig_port);
  
      $smtpconfig_user = esc_attr($_POST["smtpconfig_user"]);   
      update_option("smtpconfig_user", $smtpconfig_user);
  
      $smtpconfig_password = esc_attr($_POST["smtpconfig_password"]);   
      update_option("smtpconfig_password", $smtpconfig_password);
  
      $smtpconfig_security = esc_attr($_POST["smtpconfig_security"]);   
      update_option("smtpconfig_security", $smtpconfig_security);
  
      $smtpconfig_from = esc_attr($_POST["smtpconfig_from"]);   
      update_option("smtpconfig_from", $smtpconfig_from);
  
      $smtpconfig_fromname = esc_attr($_POST["smtpconfig_fromname"]);   
      update_option("smtpconfig_fromname", $smtpconfig_fromname);
  
      echo '<h3>Dane zostały zaktualizowane.</h3>';
      // Send test after all options are set
      if (ryju_smtpconfig_send_test()) {
        echo '<h2>Na adres twojego użytkownika została wysłana wiadomość testowa</h2>';
      } else {
        echo '<h2>Nie udało się wysłać wiadomości testowej. Konfiguracja SMTP może być niepoprawna.</h2>';
      }
    } else if ($action==='test') {
      if (ryju_smtpconfig_send_test()) {
        echo '<h2>Na adres twojego użytkownika została wysłana wiadomość testowa</h2>';
      } else {
        echo '<h2>Nie udało się wysłać wiadomości testowej. Konfiguracja SMTP może być niepoprawna.</h2>';
      }
    }
  }

  // Render the smtpconfig's options form
  ?>

  <h1>Ryju SMTP Config</h1>
  <h2>Konfiguracja SMTP</h2>
  <p>Zostawić puste dla domyślnej konfiguracji serwera</p>
  <form method="post" action="?page=ryju_smtpconfig_options" style="float: left">
    <input type="hidden" name="action" value="test">
    <button type="submit" class="button">Wyślij wiadomość testową</button>
  </form><br><br>
  <form class="form" method="post" action="?page=ryju_smtpconfig_options">
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_host">Host</label>
      <input type="text" class="form-control" name="smtpconfig_host" id="smtpconfig_host" value="<?php echo get_option("smtpconfig_host"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_smtpauth">SMTPAuth</label>
      <input type="text" class="form-control" name="smtpconfig_smtpauth" id="smtpconfig_smtpauth" value="<?php echo get_option("smtpconfig_smtpauth"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_port">Port</label>
      <input type="text" class="form-control" name="smtpconfig_port" id="smtpconfig_port" value="<?php echo get_option("smtpconfig_port"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_user">Użytkownik</label>
      <input type="text" class="form-control" name="smtpconfig_user" id="smtpconfig_user" value="<?php echo get_option("smtpconfig_user"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_password">Hasło</label>
      <input type="password" class="form-control" name="smtpconfig_password" id="smtpconfig_password" value="<?php echo get_option("smtpconfig_password"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_security">Zabezpieczenie</label>
      <input type="text" class="form-control" name="smtpconfig_security" id="smtpconfig_security" value="<?php echo get_option("smtpconfig_security"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_from">Adres (From)</label>
      <input type="text" class="form-control" name="smtpconfig_from" id="smtpconfig_from" value="<?php echo get_option("smtpconfig_from"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="smtpconfig_fromname">Od kogo (FromName)</label>
      <input type="text" class="form-control" name="smtpconfig_fromname" id="smtpconfig_fromname" value="<?php echo get_option("smtpconfig_fromname"); ?>">
    </div>
    <input type="hidden" name="action" value="update">
    <button type="submit" class="button button-primary">Zapisz zmiany</button>
  </form>
  <?php
}

/**
 * Initialize an SMTP configuration taken from options
 *
 * @since ryju-smtpc-onfig 1.0.0
 * 
 * @param  object $phpmailer PHPMailer class from Wordpress
 * @return void
 */
function ryju_smtpconfig_smtp_init( $phpmailer )
{
    // Define that we are sending with SMTP
    $phpmailer->isSMTP();
 
    // The hostname of the mail server
    if (false!==(get_option('smtpconfig_host'))) {
      $phpmailer->Host = get_option('smtpconfig_host');
    }
 
    // Use SMTP authentication (true|false)
    if (false!==(get_option('smtpconfig_smtpauth'))) {
      if (get_option('smtpconfig_smtpauth')==='true') {
        $phpmailer->SMTPAuth = true;
      } else {
        $phpmailer->SMTPAuth = false;
      }
    }
 
    // SMTP port number - likely to be 25, 465 or 587
    if (false!==(get_option('smtpconfig_port'))) {
      $phpmailer->Port = get_option('smtpconfig_port');
    }
 
    // Username to use for SMTP authentication
    if (false!==(get_option('smtpconfig_user'))) {
      $phpmailer->Username = get_option('smtpconfig_user');
    }
 
    // Password to use for SMTP authentication
    if (false!==(get_option('smtpconfig_password'))) {
      $phpmailer->Password = get_option('smtpconfig_password');
    }
 
    // The encryption system to use - ssl (deprecated) or tls
    if (false!==(get_option('smtpconfig_security'))) {
      $phpmailer->SMTPSecure = get_option('smtpconfig_security');
    }

    // From who is email? Let's fake it a little bit if we need
    if (false!==(get_option('smtpconfig_from'))) {
      $phpmailer->From = get_option('smtpconfig_from');
    }

    // What's the name of the sender? Let's fake it a little bit if we need
    if (false!==(get_option('smtpconfig_fromname'))) {
      $phpmailer->FromName = get_option('smtpconfig_fromname');
    }
}
// Add while PHPMailer initialization
add_action('phpmailer_init','ryju_smtpconfig_smtp_init');

/**
 * Initializes admin menus and pages for smtpconfig
 *
 * @since ryju-smtpc-onfig 1.0.0
 * 
 * @return void
 */
function ryju_smtpconfig_admin_menu() {
  // Add options page
  add_options_page( 'smtpconfig Settings', 'Ryju SMTP Config', 'manage_options', 'ryju_smtpconfig_options', 'ryju_smtpconfig_options' );
}
// Adds smtpconfig initialization's function while admin_menu initialization
add_action( 'admin_menu', 'ryju_smtpconfig_admin_menu' );