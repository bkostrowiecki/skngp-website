<?php
/**
* Plugin Name: Ryju Newsletter
* Plugin URI: no URI
* Description: Newsletter mechanism.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
*/

/**
 * ryju Newsletter functions.
 *
 * @package WordPress
 * @subpackage ryju
 * @since ryj-newsletteru 1.0.0
 *
 * @author  Bartosz Kostrowiecki <bartek@webstars.pl>
 */

require_once('helpers.php');

// Define newsletter log file path.
define(NEWSLETTER_LOG, WP_ADMIN_DIR.'/newsletter_log.txt');

/**
 * Register the table for newsletter
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_register_newsletter_table() {
  global $wpdb;
  $wpdb->newsletter = "{$wpdb->prefix}newsletter";
}
// Added while initializing page
add_action( 'init', 'ryju_register_newsletter_table', 1 );

/**
 * Startes logging
 * @return [type] [description]
 */
function ryju_newsletter_startlog() {
  global $newsletter_log;
  $newsletter_log = fopen(NEWSLETTER_LOG, 'a');
}

/**
 * Logs informations given by parameters
 * @param  string $str String that needs to be written in the log file
 * @return void
 */
function ryju_newsletter_log($str) {
  global $newsletter_log;
  fputs($newsletter_log, $str);
  fflush($newsletter_log);
}

/**
 * Finishes logging
 * @return void
 */
function ryju_newsletter_finishlog() {
  global $newsletter_log;
  fclose($newsletter_log);
}

/**
 * Parsing the message.
 *   There are 2 special shortcodes:
 *   - [confirm] - It generates confirmation link with the code and the email address.
 *   - [unsubscribe] - It generates unsubscribe link with the code and the email address
 *
 * @since  ryj-newsletteru 1.0.0
 * 
 * @param  string $str   The template string to parse
 * @param  string $code  The special code which has to be put in the activation link
 * @param  string $email The email address which has to be put in the activation link
 * @return string        The result of parsing.
 */
function ryju_newsletter_apply_varibles($str, $code, $email) {
  $str = str_replace('[confirm]', get_site_url()."/".get_option('newsletter_activation_page_link')."/?code=$code&email=$email", $str);
  return str_replace('[unsubscribe]', get_site_url()."/".get_option('newsletter_unsubscribe_page_link')."/?code=$code&email=$email", $str);
}

/**
 * Recive the new email address and add it to the database
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_reciver() {
  global $wpdb;

  // Get mail from POST, generate special code and set up initial result
  $email = htmlspecialchars($_POST['email']);
  $code = generate_random_string(24);
  $result = '<h4>Twój adres e-mail<br> '.$email.' został dodany do bazy subskrypcji!<br> Na twój adres została wysłana wiadomość z linkiem aktywacyjnym.<h4>';

  // @todo: mail validation is needed!
  $email = strtolower($email);
  
  // Get the row with the mail from POST
  $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE email='$email'");
  
  // If there is no such mail
  if ($row!==null) {
    // Break the function flow and print error message
    $result = 'Podany adres e-mail już istnieje w bazie subskrypcji.';
    echo $result;
    die();
  }

  // Insert new row
  if ($wpdb->insert($wpdb->prefix.'newsletter', 
      array(
        'email' => $email,
        'confirmed' => '0',
        'code' => $code
      ),
      array(
        '%s',
        '%s',
        '%s' 
      )
      // If it fails
    )==FALSE) {
    // Break the function flow and print error message
    $result = 'Wystąpił błąd podczas dodawania twojego maila do bazy subskrypcji. Sprawdź połączenie internetowe.';
    echo $result;
    die();
  }

  $msgTitle = get_option('newsletter_act_template_title');
  $msgContent = get_option('newsletter_act_template_content');

  $msgContent = ryju_newsletter_apply_varibles($msgContent, $code, $email);

  // Send the initial subscribtion message to user
  if (!ryju_newsletter_send_email($msgTitle, $msgContent, $email)) {
    // If the error appears
    // Break the function flow and print error message
    $result = 'Wystąpił błąd podczas aktywacji subskrypcji. Skontaktuj się z administratorem.';
    echo $result;
    die();
  }

  // Send successful result to the client
  echo $result;
  die();
}
// Adds it to the ajax loader
add_action('wp_ajax_addNewMailUser', 'ryju_newsletter_reciver');
add_action('wp_ajax_nopriv_addNewMailUser', 'ryju_newsletter_reciver');

/**
 * Confirm the email from user, params taken from GET
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return boolean Returns ture when confirmation is successful, otherway returns false
 */
function ryju_newsletter_confirm_email() {
  global $wpdb;

  // Escape GET varibles
  $code = htmlspecialchars($_GET['code']);
  $email = htmlspecialchars($_GET['email']);

  // Get the row with data taken from GET
  $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE email='$email' AND code='$code'");
  if ($row===null) {
    // If there is no such record
    // Return false
    return false;
  } else {
    // If there is some, try to update it setting confirmed to true (1)
    if (false===$wpdb->update($wpdb->newsletter, array('confirmed' => 1), array('id' => $row->id))) {
      // If it failed, return false
      return false;
    }
  }

  // Return true on success
  return true;
}

/**
 * Delete the email from user, params taken from GET
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return boolean Returns ture when removing is successful, otherway returns false
 */
function ryju_newsletter_unsubscribe_email() {
  global $wpdb;

  // Escape GET varibles
  $code = htmlspecialchars($_GET['code']);
  $email = htmlspecialchars($_GET['email']);

  // Get the row with data taken from GET
  $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE email='$email' AND code='$code'");
  if ($row===null) {
    // If there is no such record
    // Return false
    return false;
  } else {
    // If there is some, try to delete it
    if (false===$wpdb->delete($wpdb->newsletter, array('id' => $row->id))) {
      // If it failed, return false
      return false;
    }
  }

  // Returns true on success
  return true;
}

/**
 * Initialize an SMTP configuration taken from options
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @param  object $phpmailer PHPMailer class from Wordpress
 * @return void
 */
function ryju_newsletter_smtp_init( $phpmailer )
{
    // Define that we are sending with SMTP
    $phpmailer->isSMTP();
 
    // The hostname of the mail server
    if (false!==(get_option('newsletter_host'))) {
      $phpmailer->Host = get_option('newsletter_host');
    }
 
    // Use SMTP authentication (true|false)
    if (false!==(get_option('newsletter_smtpauth'))) {
      if (get_option('newsletter_smtpauth')==='true') {
        $phpmailer->SMTPAuth = true;
      } else {
        $phpmailer->SMTPAuth = false;
      }
    }
 
    // SMTP port number - likely to be 25, 465 or 587
    if (false!==(get_option('newsletter_port'))) {
      $phpmailer->Port = get_option('newsletter_port');
    }
 
    // Username to use for SMTP authentication
    if (false!==(get_option('newsletter_user'))) {
      $phpmailer->Username = get_option('newsletter_user');
    }
 
    // Password to use for SMTP authentication
    if (false!==(get_option('newsletter_password'))) {
      $phpmailer->Password = get_option('newsletter_password');
    }
 
    // The encryption system to use - ssl (deprecated) or tls
    if (false!==(get_option('newsletter_security'))) {
      $phpmailer->SMTPSecure = get_option('newsletter_security');
    }

    // From who is email? Let's fake it a little bit if we need
    if (false!==(get_option('newsletter_from'))) {
      $phpmailer->From = get_option('newsletter_from');
    }

    // What's the name of the sender? Let's fake it a little bit if we need
    if (false!==(get_option('newsletter_fromname'))) {
      $phpmailer->FromName = get_option('newsletter_fromname');
    }
}
// Add while PHPMailer initialization
add_action('phpmailer_init','ryju_newsletter_smtp_init');

/**
 * Send the email to a user using wp_mail
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @param  string  $title      Title of the email
 * @param  string  $content    Content of the email
 * @param  string  $toWho      Email address that will get the message
 * @param  boolean/string $attachment False, if there is no attachment, true if there is some
 * @return boolean             Returns true, if sending is successful, otherway returns false
 */
function ryju_newsletter_send_email($title, $content, $toWho, $attachment = false) {
  if (!$attachment) {
    $result = wp_mail($toWho, $title, $content);
  } else {
    // Header's setting is needed because of attachment
    $headers = 'From: '.get_option('newsletter_fromname').' <'.get_option('newsletter_from').'>'."\r\n";
    $result = wp_mail($toWho, $title, $content, array(), array($attachment));
  }
  return $result;
}

/**
 * Send the test email to the currently logged in user
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return boolean Returns true if sending message was successful, otherway false
 */
function ryju_newsletter_send_test() {
  global $current_user;

  get_currentuserinfo();
  $toWho = $current_user->user_email;

  if (!empty($toWho)) {
    return ryju_newsletter_send_email('Wiadomość testowa od '.get_bloginfo('name'), 'To jest wiadomość testowa od '.get_bloginfo('name').'. Konfiguracja SMTP jest poprawna.', $toWho);
  }
  return false;
}

/**
 * Function that gets options from the admin and save it as options to a database
 * 
 * If there is no "update_settings" sent by POST, then it only renders a form.
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_options() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

  if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  // Updates options in Wordpress if update_settings is recived from POST
  if (isset($action)) {
    if ($action==='update') {
      $newsletter_host = esc_attr($_POST["newsletter_host"]);   
      update_option("newsletter_host", $newsletter_host);
  
      $newsletter_smtpauth = esc_attr($_POST["newsletter_smtpauth"]);   
      update_option("newsletter_smtpauth", $newsletter_smtpauth);
  
      $newsletter_port = esc_attr($_POST["newsletter_port"]);   
      update_option("newsletter_port", $newsletter_port);
  
      $newsletter_user = esc_attr($_POST["newsletter_user"]);   
      update_option("newsletter_user", $newsletter_user);
  
      $newsletter_password = esc_attr($_POST["newsletter_password"]);   
      update_option("newsletter_password", $newsletter_password);
  
      $newsletter_security = esc_attr($_POST["newsletter_security"]);   
      update_option("newsletter_security", $newsletter_security);
  
      $newsletter_from = esc_attr($_POST["newsletter_from"]);   
      update_option("newsletter_from", $newsletter_from);
  
      $newsletter_fromname = esc_attr($_POST["newsletter_fromname"]);   
      update_option("newsletter_fromname", $newsletter_fromname);
  
      echo '<h3>Dane zostały zaktualizowane.</h3>';
      // Send test after all options are set
      if (ryju_newsletter_send_test()) {
        echo '<h2>Na adres twojego użytkownika została wysłana wiadomość testowa</h2>';
      } else {
        echo '<h2>Nie udało się wysłać wiadomości testowej. Konfiguracja SMTP może być niepoprawna.</h2>';
      }
    } else if ($action==='test') {
      if (ryju_newsletter_send_test()) {
        echo '<h2>Na adres twojego użytkownika została wysłana wiadomość testowa</h2>';
      } else {
        echo '<h2>Nie udało się wysłać wiadomości testowej. Konfiguracja SMTP może być niepoprawna.</h2>';
      }
    } else if ($action==='update_links') {
      $newsletter_activation_page_link = esc_attr($_POST["newsletter_activation_page_link"]);   
      update_option("newsletter_activation_page_link", $newsletter_activation_page_link);
  
      $newsletter_unsubscribe_page_link = esc_attr($_POST["newsletter_unsubscribe_page_link"]);   
      update_option("newsletter_unsubscribe_page_link", $newsletter_unsubscribe_page_link);
  
      echo '<h3>Dane stron zostały zaktualizowane.</h3>';
    }
  }

  // Render the Newsletter's options form
  ?>

  <h1>Newsletter</h1>
  <h2>Konfiguracja SMTP</h2>
  <p>Zostawić puste dla domyślnej konfiguracji serwera</p>
  <form method="post" action="?page=ryju_newsletter_options" style="float: left">
    <input type="hidden" name="action" value="test">
    <button type="submit" class="button">Wyślij wiadomość testową</button>
  </form><br><br>
  <form class="form" method="post" action="?page=ryju_newsletter_options">
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_host">Host</label>
      <input type="text" class="form-control" name="newsletter_host" id="newsletter_host" value="<?php echo get_option("newsletter_host"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_smtpauth">SMTPAuth</label>
      <input type="text" class="form-control" name="newsletter_smtpauth" id="newsletter_smtpauth" value="<?php echo get_option("newsletter_smtpauth"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_port">Port</label>
      <input type="text" class="form-control" name="newsletter_port" id="newsletter_port" value="<?php echo get_option("newsletter_port"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_user">Użytkownik</label>
      <input type="text" class="form-control" name="newsletter_user" id="newsletter_user" value="<?php echo get_option("newsletter_user"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_password">Hasło</label>
      <input type="password" class="form-control" name="newsletter_password" id="newsletter_password" value="<?php echo get_option("newsletter_password"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_security">Zabezpieczenie</label>
      <input type="text" class="form-control" name="newsletter_security" id="newsletter_security" value="<?php echo get_option("newsletter_security"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_from">Adres (From)</label>
      <input type="text" class="form-control" name="newsletter_from" id="newsletter_from" value="<?php echo get_option("newsletter_from"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_fromname">Od kogo (FromName)</label>
      <input type="text" class="form-control" name="newsletter_fromname" id="newsletter_fromname" value="<?php echo get_option("newsletter_fromname"); ?>">
    </div>
    <input type="hidden" name="action" value="update">
    <button type="submit" class="button button-primary">Zapisz zmiany</button>
  </form>
  <h2>Ustawienia linków stron aktywacyjnych</h2>
  <form class="form" method="post" action="?page=ryju_newsletter_options">
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_activation_page_link">Link strony aktywacyjnej</label>
      <input type="text" class="form-control" name="newsletter_activation_page_link" id="newsletter_activation_page_link" value="<?php echo get_option("newsletter_activation_page_link"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; display: block; float: left;" for="newsletter_unsubscribe_page_link">Link strony dezaktywacyjnej</label>
      <input type="text" class="form-control" name="newsletter_unsubscribe_page_link" id="newsletter_unsubscribe_page_link" value="<?php echo get_option("newsletter_unsubscribe_page_link"); ?>">
    </div>
    <input type="hidden" name="action" value="update_links">
    <button type="submit" class="button button-primary">Zapisz zmiany</button>
  </form>

  <?php
}

/**
 * Renders form for editing single mail row from table "newsletter"
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @param  array $data Array with data from one row of "newsletter" table
 * @return void
 */
function ryju_newsletter_edit_form($data) {
  ?>
      <h1>Edytuj email</h1>
      <form action="?page=ryju_newsletter_actions" method="post">
        <div class="form-group">
          <label style="width: 150px; display: block; float: left;" for="newsletter_fake_id" style="width: 150px">ID</label>
          <input type="text" class="form-control" name="newsletter_fake_id" id="newsletter_fake_id" value="<?php echo $data->id; ?>" disabled>
        </div>
        <div class="form-group">
          <label style="width: 150px; display: block; float: left;" for="newsletter_email" style="width: 150px">E-mail</label>
          <input type="email" class="form-control" name="newsletter_email" id="newsletter_email" value="<?php echo $data->email; ?>">
        </div>
        <div class="form-group">
          <label style="width: 150px; display: block; float: left;" for="newsletter_confirmed" style="width: 150px">Potwierdzony</label>
          <input type="text" class="form-control" name="newsletter_confirmed" id="newsletter_confirmed" value="<?php echo $data->confirmed; ?>">
          <p>0 - niepotwierdzony, 1 - potwierdzony</p>
        </div>
        <input type="hidden" name="action" value="confirm_edit">
        <input type="hidden" name="newsletter_id" value="<?php echo $data->id; ?>">
        <button type="submit">Zatwierdź zmiany</button>
      </form>
  <?php
}

/**
 * Renders the form for sending message
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_send_form() {
  ?>
      <h1>Roześlij wiadomość</h1>
      <form action="?page=ryju_newsletter_send_message" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="newsletter_title" style="width: 15%; display: block; float: left">Tytuł</label>
          <input type="text" class="form-control" style="width: 70%" name="newsletter_title" id="newsletter_title">
        </div>
        <div class="form-group">
          <label for="newsletter_content" style="width: 15%; display: block; float: left">Tekst</label>
          <textarea class="form-control" name="newsletter_content"  style="width: 70%; min-height: 400px; resize: vertical" id="newsletter_content"></textarea>
        </div>
        <div class="form-group">
          <label for="newsletter_attachment" style="width: 15%; display: block; float: left">Załącznik</label>
          <input type="file" name="newsletter_attachment" id="newsletter_attachment">
        </div>
        <input type="hidden" name="action" value="send">
        <button type="submit" style="margin-left: 15%" class="button button-primary">Roześlij wiadomość</button>
      </form>
  <?php
}

/**
 * This function is responsible for all actions on mailing list and single rows of "newsletter" table
 * 
 * Actions:
 *   edit - Renders edit form for single row
 *   confirm_edit - Confirms edit for single row with data taken from POST sended by edit form for single row (edit)
 *   delete - Renders delete form for single row
 *   confirm_delete - Confirms delete for single row with data taken from POST sended by delete form for single row (delete)
 *   clean - Delete all unconfirmed emails from mailing list
 * If there is no action parameter given by GET or POST, it simply renders a list with ryju_newsletter_list
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_actions() {
  global $wpdb;

  // Set action varible from GET or POST, GET is more important
  if (isset($_GET['action'])) {
    $action = htmlspecialchars($_GET['action']);
  } else if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  // If action varible is set
  if (isset($action)) {

    // Set id varible from GET or POST, GET is more important
    if (isset($_GET['id'])) {
      $id = htmlspecialchars($_GET['id']);
    } else if (isset($_POST['id'])) {
      $id = htmlspecialchars($_POST['id']);
    }

    // Edit action - renders form
    if ($action==='edit'){
      $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE id=$id");
      ryju_newsletter_edit_form($row);
    }
    // Confirm edit action - update database with data from POST
    else if ($action==='confirm_edit'){
      $wpdb->update($wpdb->newsletter, array(
        'email' => htmlspecialchars($_POST['newsletter_email']), 
        'confirmed' => htmlspecialchars($_POST['newsletter_confirmed'])
        ),
        array('id' => htmlspecialchars($_POST['newsletter_id']))
      );
      echo '<h1>Dane maila '.htmlspecialchars($_POST['newsletter_email']).' zostały zaktualizowane!</h1>';
    }
    // Delet action - renders form
    else if ($action==='delete') {
      $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE id=$id");

      echo '<h2>Czy na pewno chcesz usunąć adres email '.$row->email.' z newslettera?</h2>';
      echo '<a class="button button-primary" href="/wp-admin/admin.php?page=ryju_newsletter_actions&action=confirm_delete&id='.$id.'">Tak</a> ';
      echo '<a class="button" href="/wp-admin/admin.php?page=ryju_newsletter_actions">Nie</a>';
    } 
    // Confirm delete action - delete row with data from GET
    else if ($action==='confirm_delete') {
      $row = $wpdb->get_row("SELECT * FROM $wpdb->newsletter WHERE id=$id");
      $wpdb->delete($wpdb->newsletter, array('id' => $id));
      echo '<h2>Usunięto adres '.$row->email.' z newslettera</h2>';
    } 
    // Clean action - delete all rows with column "confirmed" set to 0
    else if ($action==='clean') {
      $count = $wpdb->delete($wpdb->newsletter, array('confirmed' => 0));
      echo '<h2>Usunięto niepowietrzone adresy: '.$count;
    }
  } 
  // If action varible is not set
  else {
    // Render mailing list
    ryju_newsletter_list();
  }
}

/**
 * Sends the broadcast message for all confirmed mails from mailing list
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_send_message() {
  global $wpdb;

  if (isset($_GET['action'])) {
    $action = htmlspecialchars($_GET['action']);
  } else if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  if (!isset($action)) {
    ryju_newsletter_send_form();
  } else if ($action==='send') {
    $error = false;
    $count = 0;
    $attachment = false;

    ryju_newsletter_startlog();

    if (isset($_FILES)) {
      if ($_FILES['newsletter_attachment']['tmp_name'] !== '') {
        $attachment = WP_CONTENT_DIR.'/attachments/'.$_FILES['newsletter_attachment']['name'];
        move_uploaded_file($_FILES['newsletter_attachment']['tmp_name'], $attachment);
  
        if ($_FILES['newsletter_attachment']['size']>2048000) {
          $error = true;
        }
      }
    }

    $results = $wpdb->get_results("SELECT * FROM $wpdb->newsletter WHERE confirmed=1");

    $msgTitle = htmlspecialchars($_POST['newsletter_title']);
    $msgContent = htmlspecialchars($_POST['newsletter_content'])."\n";
    $msgFooterTemplate = get_option('newsletter_footer_template');

    foreach ($results as $row) {
      $code = $row->code;
      $email = $row->email;
      $msgFooter = ryju_newsletter_apply_varibles($msgFooterTemplate, $code, $email);
      if ($error) {
        break;
      }
      if (!ryju_newsletter_send_email($msgTitle, $msgContent.$msgFooter, $row->email, $attachment)) {
        $error = true;
        ryju_newsletter_log('Failed on '.$email);
        break;
      }
      ryju_newsletter_log('Email sent sucessfully to '.$email);
      $count++;
    }

    // if (false!==$attachment) {
    //   unlink($attachment);
    // }

    if ($error) {
      echo '<h1>Wystąpił błąd przy wysyłaniu maili.</h1>';
      echo '<h2>Liczba wysłanych wiadomości: '.$count.'</h2>';
    } else {
      echo '<h1>Wysyłanie maili zakończyło się pomyślnie!</h1>';
      echo '<h2>Liczba wysłanych wiadomosci: '.$count.'</h2>';
    }
    ryju_newsletter_finishlog();
  }
}

/**
 * Renders the mailing list
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_list() {
  // Require class Newsletter_List_Table
  require_once('newsletter_list.php');
  ?>

  <h1>Lista mailingowa</h1>

  <div class="table-responsive" style="padding: 0 30px 0 0; min-width: 600px; overflow: hidden; overflow-y: auto">
  <?php

  // Prepare data to display
  $wp_list_table = new Newsletter_List_Table();
  $wp_list_table->prepare_items();

  // Display it
  $wp_list_table->display();
  ?>
  </div>
  <?php
}

/**
 * Render the activation template form
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_activation_form() {
  ?>
      <h1>Szablon wiadomości powitalnej</h1>
      <p>Użyj znaczników [confirm] oraz [unsubscribe] aby określić położenie linków aktywacyjnych</p>
      <form action="?page=ryju_newsletter_activation_template" method="post">
        <div class="form-group">
          <label for="newsletter_title" style="width: 15%; display: block; float: left">Tytuł</label>
          <input type="text" class="form-control" style="width: 70%" name="newsletter_title" id="newsletter_title" value="<?php echo get_option('newsletter_act_template_title'); ?>">
        </div>
        <div class="form-group">
          <label for="newsletter_content" style="width: 15%; display: block; float: left">Tekst</label>
          <textarea class="form-control" name="newsletter_content"  style="width: 70%; min-height: 400px; resize: vertical" id="newsletter_content"><?php echo get_option('newsletter_act_template_content'); ?></textarea>
        </div>
        <input type="hidden" name="action" value="update">
        <button type="submit" style="margin-left: 15%" class="button button-primary">Zapisz zmiany</button>
      </form>
  <?php
}

/**
 * Edits the activation template for Newsletter
 *
 * This template is used when the activation mail with link is sent
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_activation_template() {
  if (isset($_GET['action'])) {
    $action = htmlspecialchars($_GET['action']);
  } else if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  if (isset($action)) {
    if ($action==='update') {
      update_option('newsletter_act_template_title', htmlspecialchars($_POST['newsletter_title']));
      update_option('newsletter_act_template_content', htmlspecialchars($_POST['newsletter_content']));
      echo '<h3>Zmiany zostały zapisane</h3>';
    }
  } 

  ryju_newsletter_activation_form();
}

/**
 * Renders the form for the footer template
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_footer_form() {
  ?>
      <h1>Szablon stopki</h1>
      <p>Użyj znaczników [confirm] oraz [unsubscribe] aby określić położenie linków aktywacyjnych</p>
      <form action="?page=ryju_newsletter_footer_template" method="post">
        <div class="form-group">
          <label for="newsletter_footer" style="width: 15%; display: block; float: left">Tekst</label>
          <textarea class="form-control" name="newsletter_footer"  style="width: 70%; min-height: 400px; resize: vertical" id="newsletter_footer"><?php echo get_option('newsletter_footer_template'); ?></textarea>
        </div>
        <input type="hidden" name="action" value="update">
        <button type="submit" style="margin-left: 15%" class="button button-primary">Zapisz zmiany</button>
      </form>
  <?php
}

/**
 * Edits the footer template content
 *
 * The template is added to all mails and news which are send through the send message form
 * 
 * @since ryju-newsletter 1.0.0
 * 
 * @return void
 */
function ryju_newsletter_footer_template() {
  if (isset($_GET['action'])) {
    $action = htmlspecialchars($_GET['action']);
  } else if (isset($_POST['action'])) {
    $action = htmlspecialchars($_POST['action']);
  }

  if (isset($action)) {
    if ($action==='update') {
      update_option('newsletter_footer_template', htmlspecialchars($_POST['newsletter_footer']));
      echo '<h3>Zmiany zostały zapisane</h3>';
    }
  } 

  ryju_newsletter_footer_form();
}

/**
 * Initializes admin menus and pages for Newsletter
 *
 * @since ryju-newsletter 1.0.0
 * 
 * @return [type] [description]
 */
function ryju_newsletter_admin_menu() {
  // Add options page
  add_options_page( 'Newsletter Settings', 'Newsletter', 'manage_options', 'ryju_newsletter_options', 'ryju_newsletter_options' );

  // Add admin menu page
  add_menu_page( 'Newsletter', 'Newsletter', 'publish_posts', 'ryju_newsletter_actions', 'ryju_newsletter_actions', 'dashicons-email-alt', 24);
 
  // Add sumenus to menu page
  add_submenu_page('ryju_newsletter_actions', 'Lista mailingowa', 'Lista mailingowa', 'publish_posts', 'ryju_newsletter_list', 'ryju_newsletter_list'); 
  add_submenu_page('ryju_newsletter_actions', 'Wyślij wiadomość', 'Wyślij wiadomość', 'publish_posts', 'ryju_newsletter_send_message', 'ryju_newsletter_send_message'); 
  add_submenu_page('ryju_newsletter_actions', 'Edytuj szablon aktywacyjny', 'Edytuj szablon aktywacyjny', 'publish_posts', 'ryju_newsletter_activation_template', 'ryju_newsletter_activation_template'); 
  add_submenu_page('ryju_newsletter_actions', 'Edytuj szablon stopki', 'Edytuj szablon stopki', 'publish_posts', 'ryju_newsletter_footer_template', 'ryju_newsletter_footer_template'); 

  // Delete accidently created submenu
  remove_submenu_page('ryju_newsletter_actions', 'ryju_newsletter_actions');
}
// Adds Newsletter initialization's function while admin_menu initialization
add_action( 'admin_menu', 'ryju_newsletter_admin_menu' );

function ryju_newsletter_render() {
  return '<form class="form form--newsletter jsFormNewsletter" action="">
              <div class="form-group">
                <input type="email" placeholder="e-mail" class="input input--email" name="email"><br>
              </div>
              <input type="hidden" name="action" value="addNewMailUser"/>
              <button class="btn btn--send">Wyślij</button>
            </form>';
}