<?php
/**
 * Carsona Helpers functions.
 *
 * @package WordPress
 * @subpackage Carsona
 * @since Carsona 1.0.0
 *
 * @author  Bartosz Kostrowiecki <bartek@webstars.pl>
 */

/**
 * Generate the random string with length given by the parameter
 *
 * @since Carsona 1.0.0
 * 
 * @param  integer $length Length of the generated string
 * @return string The generated string
 */
function generate_random_string($length = 10) {
  return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

/**
 * Get the user's role in the Wordpress
 *
 * @since Carsona 1.0.0
 * 
 * @return string The user's role in the Wordpress
 */
function get_user_role() {
  global $current_user;

  $user_roles = $current_user->roles;
  $user_role = array_shift($user_roles);

  return $user_role;
}

if ( defined('WP_CONTENT_DIR') && !defined('WP_ADMIN_DIR') ){
  define('WP_ADMIN_DIR', str_replace('wp-content', 'wp-admin', WP_CONTENT_DIR));
}