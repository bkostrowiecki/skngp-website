
require(['esri/map', 'dojo/domReady!'], function(Map) {
  if (document.getElementById('map') !== null) {
    var map = new Map('map', {
      center: [RyjuArcMapConfig.centerX, RyjuArcMapConfig.centerY],
      zoom: RyjuArcMapConfig.zoom,
      basemap: RyjuArcMapConfig.basemap
    });
  }
});