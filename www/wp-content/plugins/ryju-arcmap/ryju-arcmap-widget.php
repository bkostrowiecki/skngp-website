<?php

class Ryju_ArcMapWidget extends WP_Widget {
  function __construct() {
    parent::__construct( 
      'ryju_arcmap_widget',
      __( 'Ryju ArcMap Widget', 'ryju-arcmap-plugin' ),
      array(
        'description' => __( 'Ryju qucikform widget, allow you to put a qucikform into a widget area', 'ryju-arcmap-plugin' ),
      )
    );
  }

  public function widget( $args, $instance ) {
    echo $args[ 'before_widget' ];
    echo do_shortcode( '[ryju_arcmap /]' );
    echo $args[ 'after_widget' ];
  }

  public function form( $instance ) {
  }

  public function update( $new_instance, $old_instance ) {
    return $instance;
  } 
};

function ryju_arcmap_register_widget() {
  register_widget( 'Ryju_ArcMapWidget' );
}

add_action( 'widgets_init', 'ryju_arcmap_register_widget' );