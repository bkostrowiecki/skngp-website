<?php

/**
* Plugin Name: Ryju ArcGIS map
* Plugin URI: no URI
* Description: That plugins created a ARCGis map
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
* Text Domain: ryju-arcmap-plugin
*/

function ryju_arcmap_setup_language() {
  if ( !load_plugin_textdomain( 'ryju-arcmap-plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' )) {
    echo '<br>No translation';
  }
}

add_action( 'plugins_loaded', 'ryju_arcmap_setup_language' );

function ryju_arcmap_setup() {
  if ( !is_admin() ) {
    wp_enqueue_style( 'esri', 'http://js.arcgis.com/3.13/esri/css/esri.css' );
  
    wp_enqueue_script( 'ryju-arcmap-plugin-head-script', plugins_url( '/js/ryju-arcmap-head.js', __FILE__ ), array(), false, false );
    wp_enqueue_script( 'arcgis', 'http://js.arcgis.com/3.13/', array(), false, false );
  
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'ryju-arcmap-plugin-script', plugins_url( '/js/ryju-arcmap.js', __FILE__ ), array( 'arcgis' ), false, true );
    wp_localize_script( 'ryju-arcmap-plugin-script', 'RyjuArcMapConfig', array( 'centerX' => get_option( 'arcmap_centerX' ), 'centerY' => get_option( 'arcmap_centerY' ), 'zoom' => get_option( 'arcmap_zoom' ), 'basemap' => get_option( 'arcmap_basemap' ) ) );
  }
}

add_action( 'init', 'ryju_arcmap_setup' );

function ryju_arcmap_shortcode_func() {
  return '<div id="map"></div>';
}

add_shortcode( 'ryju_arcmap', 'ryju_arcmap_shortcode_func' );

/**
 * Function that gets options from the admin and save it as options to a database
 * 
 * If there is no "update_settings" sent by POST, then it only renders a form.
 *
 * @since ryju-smtp-config 1.0.0
 * 
 * @return void
 */
function ryju_arcmap_options() {
  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.', 'ryju-arcmap-plugin' ) );
  }

  if ( isset( $_POST['action'] ) ) {
    $action = htmlspecialchars($_POST['action']);
  }

  // Updates options in Wordpress if update_settings is recived from POST
  if ( isset( $action ) ) {
    if ($action==='update') {
      $arcmap_centerX = esc_attr($_POST['arcmap_centerX']);   
      update_option('arcmap_centerX', $arcmap_centerX);
      $arcmap_centerY = esc_attr($_POST['arcmap_centerY']);   
      update_option('arcmap_centerY', $arcmap_centerY);
      $arcmap_zoom = esc_attr($_POST['arcmap_zoom']);   
      update_option('arcmap_zoom', $arcmap_zoom);
      $arcmap_basemap = esc_attr($_POST['arcmap_basemap']);   
      update_option('arcmap_basemap', $arcmap_basemap);
      echo '<h3>'.__( 'Settings have been updated', 'ryju-arcmap-plugin' ).'</h3>';
    }
  }

  // Render the smtpconfig's options form
  ?>

  <h1>Ryju ArcMap</h1>
  <form class="form" method="post" action="?page=ryju_arcmap_options">
    <div class="form-group">
      <label style="width: 150px; float: left;" for="arcmap_centerX"><?php _e( 'CenterX', 'ryju-arcmap-plugin' ); ?></label>
      <input type="text" class="form-control" name="arcmap_centerX" id="arcmap_centerX" value="<?php echo get_option("arcmap_centerX"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; float: left;" for="arcmap_centerY"><?php _e( 'CenterY', 'ryju-arcmap-plugin' ); ?></label>
      <input type="text" class="form-control" name="arcmap_centerY" id="arcmap_centerY" value="<?php echo get_option("arcmap_centerY"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; float: left;" for="arcmap_zoom"><?php _e( 'Zoom', 'ryju-arcmap-plugin' ); ?></label>
      <input type="text" class="form-control" name="arcmap_zoom" id="arcmap_zoom" value="<?php echo get_option("arcmap_zoom"); ?>">
    </div>
    <div class="form-group">
      <label style="width: 150px; float: left;" for="arcmap_basemap"><?php _e( 'Basemap', 'ryju-arcmap-plugin' ); ?></label>
      <input type="text" class="form-control" name="arcmap_basemap" id="arcmap_basemap" value="<?php echo get_option("arcmap_basemap"); ?>">
    </div>
    <input type="hidden" name="action" value="update">
    <button type="submit" class="button button-primary"><?php _e( 'Save changes', 'ryju-arcmap-plugin' ); ?></button>
  </form>
  <?php
}

/**
 * Initializes admin menus and pages for smtpconfig
 *
 * @since ryju-smtpc-onfig 1.0.0
 * 
 * @return void
 */
function ryju_arcmap_admin_menu() {
  // Add options page
  add_options_page( 'ArcGIS Map Settings', 'Ryju ArcMap', 'manage_options', 'ryju_arcmap_options', 'ryju_arcmap_options' );
}
// Adds smtpconfig initialization's function while admin_menu initialization
add_action( 'admin_menu', 'ryju_arcmap_admin_menu' );

require_once( 'ryju-arcmap-widget.php' );