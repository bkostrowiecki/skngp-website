<?php

class Ryju_PartialsWidget extends WP_Widget {
  function __construct() {
    parent::__construct( 
      'ryju_partials_widget',
      __( 'Ryju Partial Widget', 'ryju_partials_plugin' ),
      array(
        'description' => __( 'Ryju partial widget, allow you to put a partial into a widget area', 'ryju_partials_plugin' ),
      )
    );
  }

  public function widget( $args, $instance ) {
    echo $args[ 'before_widget' ];
    echo do_shortcode( '[partial slug='.$instance[ 'partial_name' ].']' );
    echo $args[ 'after_widget' ];
  }

  public function form( $instance ) {
    $partial_name = ! empty( $instance[ 'partial_name' ] ) ? $instance[ 'partial_name' ] : __( 'Partial name', 'ryju_partials_plugin' );
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'partial_name' ); ?>"><?php _e( 'Partial name:', 'ryju_partials_plugin' ); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'partial_name' ); ?>" name="<?php echo $this->get_field_name( 'partial_name' ); ?>" class="widefat" value="<?php echo esc_attr( $partial_name ); ?>">
    </p>
    <?php
  }

  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance[ 'partial_name' ] = ( ! empty ( $new_instance[ 'partial_name' ] ) ) ? strip_tags( $new_instance[ 'partial_name' ] ) : $old_instance[ 'old_instance' ];
    return $instance;
  } 
}

function ryju_partials_register_widget() {
  register_widget( 'Ryju_PartialsWidget' );
}

add_action( 'widgets_init', 'ryju_partials_register_widget' );