<?php

/**
* Plugin Name: Ryju Partials
* Plugin URI: no URI
* Description: That plugin creates a partial post type. You can use the partials by shortcode and by widgets.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
* Text Domain: ryju_partials_plugin
*/

function ryju_partials_setup_language() {
  if ( !load_plugin_textdomain( 'ryju_partials_plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' )) {
    echo '<br>No translation';
  }
}

add_action( 'plugins_loaded', 'ryju_partials_setup_language' );

function ryju_partials_setup() {
  register_taxonomy( 'ryju_partials_tags', 'ryju_partials_posts', array( 
    'hierarchical' => false,
    'label' => __( 'Tags', 'ryju_partials_plugin' ),
    'query_var' => true,
    'rewrite' => array( 
      'slug' => 'ryju_partials_tags',
      'with_front' => 'false'
    )
  ) );

  register_post_type( 'ryju_partials_posts', array( 
    'label' => __( 'Partial', 'ryju_partials_plugin' ),
    'labels' => array(
      'name' => __( 'Partials', 'ryju_partials_plugin' ),
      'singular_name' => __( 'Partial', 'ryju_partials_plugin' ),
      'add_new' => __( 'Add new partial', 'ryju_partials_plugin' ),
      'add_new_item' => __( 'Add new partial', 'ryju_partials_plugin' ),
      'edit_item' => __( 'Edit partial', 'ryju_partials_plugin' ),
      'new_item' => __( 'New edit partial', 'ryju_partials_plugin' ),
      'view_item' => __( 'View partial', 'ryju_partials_plugin' ),
      'search_items' => __( 'Search partials', 'ryju_partials_plugin' ),
      'not_found' => __( 'No partials found', 'ryju_partials_plugin' ),
      'not_found_in_thrash' => __( 'No partials found in thrash', 'ryju_partials_plugin' ),
      'parent_item_colon' => __( 'Parent partial:', 'ryju_partials_plugin' ),
      'menu_name' => __( 'Partials', 'ryju_partials_plugin' )
    ),
    'hierarchical' => false,
    'description' => 'Partials',
    'supports' => array( 'title', 'editor', 'author', 'custom-fields', 'revisions', 'page-attributes' ),
    'taxonomies' => array( 'ryju_partials_tags' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-admin-page',
    'show_in_nav_menus' => false,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => true,
    'capability_type' => 'page'
  ) );

  add_shortcode( 'partial', 'ryju_partial_shortcode_func' );
}

function ryju_partial_func( $slug ) {
  $partial = get_posts( array(
      'name'        => $slug,
      'post_type'   => 'ryju_partials_posts',
      'post_status' => 'publish',
      'numberposts' => 1
    ) );

    $id = 0;

    if ( $partial ) {
      $id = $partial[0]->ID;
    } else {
      throw 'Partial was not found';
    }

    return do_shortcode( $partial[0]->post_content ).edit_post_link( __( 'Edit partial', 'ryju_partials_plugin' ), '', '', $id );
}

// [bartag foo="foo-value"]
function ryju_partial_shortcode_func( $atts ) {
  $a = shortcode_atts( array(
      'slug' => 'something',
  ), $atts );
  return ryju_partial_func( $atts['slug'] );
}

require_once( 'ryju-partials-widget.php' );

add_action( 'init', 'ryju_partials_setup' );