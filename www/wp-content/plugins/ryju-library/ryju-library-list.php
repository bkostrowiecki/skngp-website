<?php
/**
 * Ryju library list functionality.
 *
 * @package WordPress
 * @subpackage Ryju
 * @since Ryju Library 1.0.0
 *
 * @author  Bartosz Kostrowiecki <bartek@webstars.pl>
 */

if(!class_exists('WP_List_Table')){
  require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

/**
 * This is the class for managing list of mails in the newsletter
 *
 * @since  Ryju Library 1.0.0
 */
class Library_List_Table extends WP_List_Table {

   /**
    * Constructor, we override the parent to pass our own arguments
    * We usually focus on three parameters: singular and plural labels, as well as whether the class supports AJAX.
    *
    * @since  Ryju Library 1.0.0
    */
  function __construct() {
    parent::__construct( array(
      'singular'=> 'wp_list_text_library', //Singular label
      'plural' => 'wp_list_text_libraries', //plural label, also this well be one of the table css class
      'ajax'   => false //We won't support Ajax for this table
    ) );
  }

  /**
  * Adds extra markup in the toolbars before or after the list
  *
  * @since  Ryju Library 1.0.0
  * 
  * @param string $which, helps you decide if you add the markup after (bottom) or before (top) the list
  * @return void
  */
  function extra_tablenav( $which ) {
    ?>
    <!-- <a class="button" href="/wp-admin/admin.php?page=ryju_library&action=clean">Usuń niepotwierdzone</a> -->
    <?php
  }

  /**
   * Returns column labels
   *
   * @since  Ryju Library 1.0.0
   * 
   * @return array Assosiate array of strings, string are column labels
   */
  function get_columns() {
    return $columns = array(
      'col_library_id' => __( 'ID', 'ryju-library-plugin' ),
      'col_library_title' => __( 'Title', 'ryju-library-plugin' ),
      'col_library_author' => __( 'Author', 'ryju-library-plugin' ),
      'col_library_publish_by' => __( 'Publish by', 'ryju-library-plugin' ),
      'col_library_publish_time' => __( 'Publish time', 'ryju-library-plugin' )
    );
  }

  /**
   * Returns names of column in database's table, that are needed for sorting
   *
   * @since  Ryju Library 1.0.0
   * 
   * @return array Assiosiate array of arrays with string and boolean, 
   * when first element of subarray is a column name from a database's table.
   */
  public function get_sortable_columns() {
    return $sortable = array(
      'col_library_id' => array('id', true),
      'col_library_title' => array('title', true),
      'col_library_author' => array('author', true),
      'col_library_publish_by' => array( 'publish_by', true ),
      'col_library_publish_time' => array( 'publish_time', true )
    );
  }

  /**
  * Prepare the table with different parameters, pagination, columns and table elements
  *
  * @since  Ryju Library 1.0.0
  *
  * @return void
  */
  function prepare_items() {
    global $wpdb, $_wp_column_headers;
    $screen = get_current_screen();
  
    /* -- Preparing your query -- */
    $query = "SELECT * FROM $wpdb->library";
  
    /* -- Ordering parameters -- */

    //Parameters that are going to be used to order the result
    $orderby = !empty($_GET["orderby"]) ? htmlspecialchars($_GET["orderby"]) : '';
    $order = !empty($_GET["order"]) ? htmlspecialchars($_GET["order"]) : '';

    if(!empty($orderby) & !empty($order)){ 
      $query.=' ORDER BY '.$orderby.' '.$order; 
    }

    /* -- Pagination parameters -- */
    //Number of elements in your table?
    $totalitems = $wpdb->query($query); //return the total number of affected rows
    //How many to display per page?
    $perpage = 25;
    //Which page is this?
    $paged = !empty($_GET["paged"]) ? htmlspecialchars($_GET["paged"]) : '';
    //Page Number
    if(empty($paged) || !is_numeric($paged) || $paged<=0 ){ 
      $paged=1; 
    }

    //How many pages do we have in total?
    $totalpages = ceil($totalitems/$perpage);

    //adjust the query to take pagination into account
    if(!empty($paged) && !empty($perpage)) {
      $offset=($paged-1)*$perpage;
      $query.=' LIMIT '.(int)$offset.','.(int)$perpage;
    }

    /* -- Register the pagination -- */
    $this->set_pagination_args( array(
       "total_items" => $totalitems,
       "total_pages" => $totalpages,
       "per_page" => $perpage,
    ) );
      //The pagination links are automatically built according to those parameters

    /* -- Register the Columns -- */
    $columns = $this->get_columns();
    $hidden = array();
    $sortable = $this->get_sortable_columns();
    $this->_column_headers = array($columns, $hidden, $sortable);

    /* -- Fetch the items -- */
    $this->items = $wpdb->get_results($query);
  }

  /**
  * Display the rows of records in the table
  * 
  * @return string, echo the markup of the rows
  */
  function display_rows() {
    //Get the records registered in the prepare_items method
    $records = $this->items;
  
    //Get the columns registered in the get_columns and get_sortable_columns methods
    list( $columns, $hidden ) = $this->get_column_info();
  
    //Loop for each record
    if(!empty($records)){

      foreach($records as $rec) {
        //Open the line
        echo '<tr id="record_'.$rec->id.'">';
        foreach ( $columns as $column_name => $column_display_name ) {
          //Style attributes for each col
          $class = "class='$column_name column-$column_name'";
          $style = "";
          if ( in_array( $column_name, $hidden ) ) {
            $style = ' style="display:none;"';
          }
          $attributes = $class.$style;
          //edit link
          // $editlink  = '/wp-admin/admin.php?page=ryju_library&action=edit&id='.(int)$rec->id;
          //delete link
          // $deletelink  = '/wp-admin/admin.php?page=ryju_library&action=delete&id='.(int)$rec->id;
          
          //Display the cell
          switch ( $column_name ) {
            case "col_library_id": 
              echo '<td '.$attributes.'>'.stripslashes($rec->id).'</td>';
            break;
            case "col_library_title":
              echo '<td '.$attributes.'>'.stripslashes($rec->title).'</td>';
            break;
            case "col_library_author":
              echo '<td '.$attributes.'>'.stripslashes($rec->author).'</td>';
            break;
            case "col_library_publish_by":
              echo '<td '.$attributes.'>'.stripslashes($rec->publish_by).'</td>';
            break;
            case "col_library_publish_time":
              echo '<td '.$attributes.'>'.stripslashes($rec->publish_by).'</td>';
            break;
            case "col_library_actions":
              echo '<td '.$attributes.'><a href='.$editlink.'>Edytuj</a> | <a href='.$deletelink.'>Usuń</a></td>';
            break;
          }
        } 
        //Close the line
        echo'</tr>';
      }
    }
  }
}