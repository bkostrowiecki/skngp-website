<?php
/**
* Plugin Name: Ryju Library
* Plugin URI: no URI
* Description: The library, books, stuff.
* Version: 1.0
* Author: Bartosz Kostrowiecki
* Author URI: http://bkostrowiecki.pl
* License: GPL12
* Text Domain: ryju-library-plugin
*/

function ryju_library_setup_language() {
  if ( !load_plugin_textdomain( 'ryju-library-plugin', false, dirname( plugin_basename(__FILE__) ) . '/languages/' )) {
    //echo '<br>No translation';
  }
}

add_action( 'plugins_loaded', 'ryju_library_setup_language' );

/**
 * Register the table for library
 *
 * @since ryju-library 1.0.0
 * 
 * @return void
 */
function ryju_register_library_table() {
  global $wpdb;
  $wpdb->library = "{$wpdb->prefix}library";
}
// Added while initializing page
add_action( 'init', 'ryju_register_library_table', 1 );

function ryju_library_book_form() {
  ?>
  <h1><?php _e( 'Add new book', 'ryju-library-plugin' ); ?></h1>
  <form class="form" method="post" action="?page=ryju_library_add">
    <div class="form-group">
      <label style="float: left; width: 150px" for="id"><?php _e( 'ID', 'ryju-library-plugin' ) ?></label>
      <div>
        <input name="id" id="id" cols="80" rows="10" value="<?php  ?>">
      </div>
    </div>
    <div class="form-group">
      <label style="float: left; width: 150px" for="title"><?php _e( 'Title', 'ryju-library-plugin' ) ?></label>
      <div>
        <input name="title" id="title" cols="80" rows="10" value="<?php  ?>">
      </div>
    </div>
    <div class="form-group">
      <label style="float: left; width: 150px" for="author"><?php _e( 'Author', 'ryju-library-plugin' ) ?></label>
      <div>
        <input name="author" id="author" cols="80" rows="10" value="<?php ?>">
      </div>
    </div>
    <div class="form-group">
      <label style="float: left; width: 150px" for="publish_by"><?php _e( 'Published by', 'ryju-library-plugin' ) ?></label>
      <div>
        <input name="publish_by" id="publish_by" cols="80" rows="10" value="<?php  ?>">
      </div>
    </div>
    <div class="form-group">
      <label style="float: left; width: 150px" for="publish_time"><?php _e( 'Publish time', 'ryju-library-plugin' ) ?></label>
      <div>
        <input name="publish_time" id="publish_time" cols="80" rows="10" value="<?php ?>">
      </div>
    </div>
    <input type="hidden" name="action" value="add_new">
    <button type="submit" class="button button-primary"><?php _e( 'Save changes', 'ryju-library-plugin' ); ?></button>
  </form>
  <?php
}

function ryju_library_add() {
  if ( array_key_exists( 'action', $_POST ) ) {
    if ( $_POST['action'] === 'add_new' ) {
      if ( $wpdb->insert($wpdb->prefix.'library', 
        array(
          'id' => $_POST[ 'id' ],
          'title' => $_POST[ 'title' ],
          'author' => $_POST[ 'author' ],
          'publish_by' => $_POST[ 'publish_by' ],
          'publish_time' => $_POST[ 'publish_time' ]
        ),
        array(
          '%s',
          '%s',
          '%s',
          '%s',
          '%s'
        )
        // If it fails
      )===FALSE) {
        // Break the function flow and print error message
        _e( 'Error occured while adding new book to the library', 'ryju-library-plugin' );
        die();
      }
    }
  }
  ryju_library_book_form();
}

/**
 * Renders the books list
 *
 * @since ryju-library 1.0.0
 * 
 * @return void
 */
function ryju_library_list() {
  // Require class Newsletter_List_Table
  require_once( 'ryju-library-list.php ');
  ?>

  <h1>Lista książek</h1>

  <div class="table-responsive" style="padding: 0 30px 0 0; min-width: 600px; overflow: hidden; overflow-y: auto">
  <?php

  // Prepare data to display
  $wp_list_table = new Library_List_Table();
  $wp_list_table->prepare_items();

  // Display it
  $wp_list_table->display();
  ?>
  </div>
  <?php
}

/**
 * Initializes admin menus and pages for Library
 *
 * @since ryju-library 1.0.0
 * 
 * @return [type] [description]
 */
function ryju_library_admin_menu() {
  // Add options page
  add_options_page( 'Library Settings', 'Library', 'manage_options', 'ryju_library_options', 'ryju_library_options' );

  // Add admin menu page
  add_menu_page( 'Library', 'Library', 'publish_posts', 'ryju_library_actions', 'ryju_library_actions', 'dashicons-email-alt', 24);
 
  // Add sumenus to menu page
  add_submenu_page( 'ryju_library_actions', __( 'Lista książek', 'SKNGP SGGW' ), __( 'Books list', 'ryju-library-plugin' ), 'publish_posts', 'ryju_library_list', 'ryju_library_list' ); 
  add_submenu_page( 'ryju_library_actions', __( 'Dodaj nową książkę', 'SKNGP SGGW' ), __( 'Add new book', 'ryju-library-plugin' ), 'publish_posts', 'ryju_library_add', 'ryju_library_add' );
  // Delete accidently created submenu
  remove_submenu_page( 'ryju_library_actions', 'ryju_library_actions' );
}
// Adds Library initialization's function while admin_menu initialization
add_action( 'admin_menu', 'ryju_library_admin_menu' );