<?php
/**
 * Podstawowa konfiguracja WordPressa.
 *
 * Ten plik zawiera konfiguracje: ustawień MySQL-a, prefiksu tabel
 * w bazie danych, tajnych kluczy i ABSPATH. Więcej informacji
 * znajduje się na stronie
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Kodeksu. Ustawienia MySQL-a możesz zdobyć
 * od administratora Twojego serwera.
 *
 * Ten plik jest używany przez skrypt automatycznie tworzący plik
 * wp-config.php podczas instalacji. Nie musisz korzystać z tego
 * skryptu, możesz po prostu skopiować ten plik, nazwać go
 * "wp-config.php" i wprowadzić do niego odpowiednie wartości.
 *
 * @package WordPress
 */

// ** Ustawienia MySQL-a - możesz uzyskać je od administratora Twojego serwera ** //
/** Nazwa bazy danych, której używać ma WordPress */
// define('DB_NAME', 'skngp-website');

/** Nazwa użytkownika bazy danych MySQL */
// define('DB_USER', 'root');

/** Hasło użytkownika bazy danych MySQL */
// define('DB_PASSWORD', '');

/** Nazwa hosta serwera MySQL */
// define('DB_HOST', 'localhost');

/** Kodowanie bazy danych używane do stworzenia tabel w bazie danych. */
define('DB_CHARSET', 'utf8');

/** Typ porównań w bazie danych. Nie zmieniaj tego ustawienia, jeśli masz jakieś wątpliwości. */
define('DB_COLLATE', '');

/**#@+
 * Unikatowe klucze uwierzytelniania i sole.
 *
 * Zmień każdy klucz tak, aby był inną, unikatową frazą!
 * Możesz wygenerować klucze przy pomocy {@link https://api.wordpress.org/secret-key/1.1/salt/ serwisu generującego tajne klucze witryny WordPress.org}
 * Klucze te mogą zostać zmienione w dowolnej chwili, aby uczynić nieważnymi wszelkie istniejące ciasteczka. Uczynienie tego zmusi wszystkich użytkowników do ponownego zalogowania się.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'xN].j|8]|92pIrb7pj)A}V=+}f|[-U)D7J~2F[9.9x;Mvg+{Qu%+y=f}?mlLIxd_');
define('SECURE_AUTH_KEY',  '/Zl-;q]vCH-v_0O,xO++Z4@8p~|FuJ~QFlv~M.S$ :<V4uHFdJGB8h_t)-+7C^ko');
define('LOGGED_IN_KEY',    'ZU)kTssF#G-pN4?/>($:C^.V,<.H}Z^HE?d?m6l8K.na:[r-}PY_Zh,`~^23Y-/a');
define('NONCE_KEY',        '(s8}@pMJ=M-^Z+9gcr/tp6RT`[U87eT:<T&$tT_(:kVxCG_pHC&fCGbN|t(#Yx&0');
define('AUTH_SALT',        ':iKw.U+N)x`jbA], )0sh7<9R`2MH|~([0(.~v&B o&<:Byt2X^7zP;?+tTC*cIu');
define('SECURE_AUTH_SALT', 'WL<p10:[oQ8otae*I_82}/UehlvMW+<Ocf&@VvB1L7iXaR:PRM_.Ho-)$[UOARlU');
define('LOGGED_IN_SALT',   'EE?[V0NXJ<pd[Nc%yxlyNwN(|`.*!RL_=72hqg:!FtLft0<)n^RV{{}~{PT*=s32');
define('NONCE_SALT',       '%t,+L-[4!L|Hw,a:+2(@j.pSz|$aSE7dqljTfw~i:O9d{c0LP{0Gf:%C+U^lBgZD');

/**#@-*/

/**
 * Prefiks tabel WordPressa w bazie danych.
 *
 * Możesz posiadać kilka instalacji WordPressa w jednej bazie danych,
 * jeżeli nadasz każdej z nich unikalny prefiks.
 * Tylko cyfry, litery i znaki podkreślenia, proszę!
 */
$table_prefix  = 'skngp_';

/**
 * Dla programistów: tryb debugowania WordPressa.
 *
 * Zmień wartość tej stałej na true, aby włączyć wyświetlanie ostrzeżeń
 * podczas modyfikowania kodu WordPressa.
 * Wielce zalecane jest, aby twórcy wtyczek oraz motywów używali
 * WP_DEBUG w miejscach pracy nad nimi.
 */
define('WP_DEBUG', false);

/* To wszystko, zakończ edycję w tym miejscu! Miłego blogowania! */

/** Absolutna ścieżka do katalogu WordPressa. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

// define('DB_FILE', 'skngp-website.sqlite3');
// define('DB_DIR', 'wp-content');
define('USE_MYSQL', false);

/** Ustawia zmienne WordPressa i dołączane pliki. */
require_once(ABSPATH . 'wp-settings.php');