# SKNGP website

Strona internetowa oparta o CMS ***Wordpress*** tworzona dla SKNGP z SGGW w ramach wolontariatu.

## Wymagania
- Serwer obsługujący ***PHP*** w wersji ***5.2.4*** lub wyższej.
- Serwer obsługujący ***MySQL*** w wersji ***5.0*** lub wyższej.
- Zainstalowany i włączony ***mod_rewrite*** na serwerze ***Apache***.

Zalecane jest używanie serwera ***Apache***.

## Struktura
- **/resources** - zawiera zasoby wewnętrzne aplikacji, niedostępne dla użytkowników
- **/www** - zawiera zasoby dostępne publicznie dla użytkowników

Do poprawnego działa aplikacji wystarczająca jest instalacja zawartości katalogu **www**.

## Zależności
- ***Wordpress*** w wersji co najmniej ***4.1***
- ***LESS*** w wersji ***2.2.0***
- ***Bootstrap*** wersja ***3.3.1***, kompilacja własna.
- ***Fontello***, wybrane ikonki.

## Komponenty
- Motyw (*Theme*) dla CMSa ***Wordpress*** w wersji ***4.1***
- Wtyczka do zarządzania social mediami dla ***Wordpress***
- Wtyczka do zarządzania biblioteczką dla ***Wordpress***

## Licencja
W obecnym stanie udzielona została licencja na użytek aplikacji wyłącznie przez SKNGP przy SGGW. Dotyczy to komponentów składających się na system. Jednakże wszelkie zależności są dostępnymi za darmo aplikacjami i bibliotekami, toteż nie podlegają restrykcjom związanym z licencją powyższego oprogramowania.